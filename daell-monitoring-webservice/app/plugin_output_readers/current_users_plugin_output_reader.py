import re

def gather_status_data_from_current_users(plugin_output):
  
	#print('plugin_output: ' + plugin_output)
  
	result = {}
	result.clear()
	
	user_count = get_user_count_from_plugin_output(plugin_output)
	user_status = get_user_status_from_plugin_output(plugin_output)
	
	result['users'] = {
		'status':user_status,
		'count': {
			'logged in': user_count,
			'existing': 'not yet supported',
		}
	}

	return result

def get_user_status_from_plugin_output(plugin_output):
	status_end_index = -1

	for i in range(len(plugin_output)):
		if not (plugin_output[i].isupper() or plugin_output[i] == '_' or plugin_output[i] == ' '):
			status_end_index = i-1
			break

	if status_end_index != -1:
		status = plugin_output[:status_end_index]

	else:
		status = 'unknown'

	return status
	
def get_user_count_from_plugin_output(plugin_output):
	user_count = re.findall('\d+', plugin_output)[0]
	return int(user_count)