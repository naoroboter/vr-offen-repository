import re

def gather_status_data_from_ping(plugin_output):
	result = {}
	#PING OK - Packet loss = 0%, RTA = 0.10 ms
	ping_latency = get_ping_latency_from_plugin_output(plugin_output)
	ping_status = get_ping_status_from_plugin_output(plugin_output)
	packet_loss = get_packet_loss_from_plugin_output(plugin_output)
	
	result['network'] = {
		'status': ping_status,
		'latency': ping_latency, #in milliseconds
		'loss_percent': packet_loss
	}
	return result

def get_ping_status_from_plugin_output(plugin_output):
	
	status_end_index = -1
	for i in range(len(plugin_output)):
		if not (plugin_output[i].isupper() or plugin_output[i] == '_' or plugin_output[i] == ' '):
			status_end_index = i-1
			break
	if status_end_index != -1:
		status = plugin_output[:status_end_index]
	else:
		status = 'unknown'
		
	#print('PING STATUS IS:|'+status+'|')
	return status
	
	
def get_ping_latency_from_plugin_output(plugin_output):
	#PING OK - Packet loss = 0%, RTA = 0.10 ms
	tmp = re.findall('RTA\s*=\s*\d+\.\d*', plugin_output)[0]
	#print(tmp)
	latency = re.findall('\d+\.\d*', tmp)[0]
	#print('LATENCY:' + latency)
	return float(latency)

def get_packet_loss_from_plugin_output(plugin_output):
	#PING OK - Packet loss = 0%, RTA = 0.10 ms
	numbers_in_string = re.findall('\d+\.?\d*', plugin_output)
	loss = str(numbers_in_string[0])
	return int(loss)