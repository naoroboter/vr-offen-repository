import re

def gather_status_data_from_remote_users(plugin_output):

	#USERS OK - 3 users currently logged in
  
	result = {}
	
	if (
		plugin_output == '(Return code of 255 is out of bounds)' or
		plugin_output.find('Socket timeout') != -1
	):
		user_count = 'NOT AVAILABLE'
		user_status = 'NOT AVAILABLE'
	else:
		user_count = get_user_count_from_remote_users(plugin_output)
		user_status = get_user_status_from_remote_users(plugin_output)
	
	result['users'] = {
		'status':user_status,
		'count': {
			'logged in': user_count,
			'existing': 'not yet supported',
		}
	}

	return result

def get_user_status_from_remote_users(plugin_output):
	status = re.findall('[[A-Z]*\s[A-Z]*]*', plugin_output)[0]
	return status
	
def get_user_count_from_remote_users(plugin_output):
	user_count = re.findall('\d+', plugin_output)[0]
	return int(user_count)