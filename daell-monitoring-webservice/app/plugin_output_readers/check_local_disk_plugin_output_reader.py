import re

def gather_status_data_from_check_local_disk(plugin_output):
	result = {}
	
	#available = False or (plugin_output[:2] == 'OK')
		
	status = get_disk_status_from_plugin_output(plugin_output)
	free_space = get_free_space_from_plugin_output(plugin_output)
	free_space_percent = get_free_space_percent_from_plugin_output(plugin_output)
	free_inodes = get_free_inodes_from_plugin_output(plugin_output)
	free_inodes_percent = get_free_inodes_percent_from_plugin_output(plugin_output)

	result['disk'] = {
		#'available':True,
		'status': status,
		'free_space': free_space,
		'free_space_percent': free_space_percent,
		'free_inodes': free_inodes,
		'free_inodes_percent': free_inodes_percent
	}
	#/usr/local/nagios/var/status.dat
	return result

def get_disk_status_from_plugin_output(plugin_output):
	# DISK OK - free space: / 31408 MB (78% inode=-)
	# (Return code of 255 is out of bounds)
	# DISK OK - free space: / 9277 MB (61% inode=95%)
	if plugin_output.find('(Return code of 255 is out of bounds)') != -1:
		status = 'NOT AVAILABLE'
	elif plugin_output.find('DISK OK') != -1:
		status = 'DISK OK'
	else:
		status = 'UNRECOGNIZED STATUS'
	return status
	
def get_free_space_from_plugin_output(plugin_output):
	# find a number with an arbitrary number of digits (\d*),
	# which is followed by an arbitrary number of whitespace characters (\s*)
	# which is followed by either KB,MB,GB or TB ([KMGT]B)
	# there should only be one occurence, so we take the first element of the returned list
	free_space = re.findall('\d*\s*[KMGT]B', plugin_output)[0]
	#print('FREE SPACE:' + free_space)
	return free_space

def get_free_space_percent_from_plugin_output(plugin_output):
	free_space_percent = re.findall('\d*%', plugin_output)[0][:-1]
	return float(free_space_percent)

def get_free_inodes_from_plugin_output(plugin_output):
	return 'NOT YET SUPPORTED'

def get_free_inodes_percent_from_plugin_output(plugin_output):
	tmp = re.findall('inode\s*=\s*\d*\s*', plugin_output)[0]
	free_inodes_percent = re.findall('\d*', tmp)[0]
	#print ('FREE INODES PERCENT: ' + free_inodes_percent)
	
	if free_inodes_percent == '':
		free_inodes_percent = 'UNKNOWN'
	
	try:
		free_inodes_percent = float(free_inodes_percent)
	except:
		free_inodes_percent = 'UNKNOWN'

	return free_inodes_percent