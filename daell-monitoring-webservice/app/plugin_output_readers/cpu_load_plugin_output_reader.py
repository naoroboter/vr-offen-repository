def gather_status_data_from_cpu_load(plugin_output):
	result = {}
	
	#available = False or (plugin_output[:2] == 'OK')
		
	status = get_cpu_status_from_plugin_output(plugin_output)
	load_avgs = get_cpu_load_average_from_plugin_output(plugin_output)
	
	result['cpu'] = {
		#'available':True,
		'status':status,
		'load average': {
			'1min': float(load_avgs[0]),
			'5min': float(load_avgs[1]),
			'15min': float(load_avgs[2])
		}
	}
	
	return result

def get_cpu_status_from_plugin_output(plugin_output):
	if plugin_output[:2] != 'OK':
		status = plugin_output
	else:
		status = plugin_output[:2]
	"""
	status_end_index = -1
	for i in range(len(plugin_output)):
		if not (plugin_output[i].isupper() or plugin_output[i] == '_'):
			status_end_index = i
			break
	
	if status_end_index != -1:
		status = plugin_output[:status_end_index]
	else:
		status = 'unknown'
	"""
	#print('CPU STATUS IS:|'+status+'|')
	return status
	
def get_cpu_load_average_from_plugin_output(plugin_output):
	load_average_start_index = plugin_output.find('load average: ') + len('load average: ')
	
	if load_average_start_index != -1:
		load_average = plugin_output[load_average_start_index:]
		load_avgs = load_average.split(sep=', ', maxsplit=-1)
		if len(load_avgs) != 3:
			load_avgs = [-1, -1, -1]
	else:
		load_avgs = [-1, -1, -1]
	
	return load_avgs