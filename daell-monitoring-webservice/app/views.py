from app import app
#from app import StatusDatParser
from flask import Flask, jsonify, request

from .status_dat_parser import StatusDatParser

@app.route('/')
@app.route('/index')
def index():
	return "Hello, World!"


#@app.route ('<machine_id>/<resource_name>/<resource_attribute_name>')

@app.route ('/get/all')
def get_information_all():
	host_status = get_status_data()
	return jsonify(result=host_status)

"""
@app.route ('/get/<machine_id>/all')
def get_machine_information_all(machine_id):
	host_status = get_status_data()
	return jsonify(result=host_status)
"""

def get_status_data():
	status_dat_parser = StatusDatParser()
	host_status = status_dat_parser.parse()
	return host_status