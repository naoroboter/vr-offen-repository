from .host_status import HostStatus
from .plugin_output_readers import *
import re


class StatusDatParser:

	SERVICE_START = 'servicestatus'
	HOST_START = 'hoststatus'

	HOST_NAME = 'host_name'
	
	service_description_to_resource_name = {
		'CPU Load':'cpu load', # e.g.: CHECK_NRPE: Socket timeout after 10 seconds. / OK - load average: 0.16, 0.11, 0.12
		'Current Load':'unkown', # e.g.: OK - load average: 0.47, 0.37, 0.33
		'Current Users':'unknown', # e.g.: USERS OK - 5 users currently logged in
		'HTTP':'unknown', # e.g.: HTTP WARNING: HTTP/1.1 403 Forbidden - 1249 bytes in 0.003 second response time
		'PING':'unknown', # e.g.: PING OK - Packet loss = 0%, RTA = 0.10 ms
		'Root Partition':'unknown', # e.g.: DISK OK - free space: / 31408 MB (78% inode=-):
		'SSH':'unknown', # e.g.: connect to address 127.0.0.1 and port 22: Connection refused
		'Swap Usage':'swap usage', # e.g.: SWAP OK - 100% free (2052 MB out of 2060 MB)
		'Total Processes':'unknown' # e.g.: PROCS OK: 72 processes with STATE = RSZDT
	}
	
	ips = {
		'TM0401':'194.95.45.141',
		'TM0402':'194.95.45.142',
		'TM0403':'194.95.45.143',
		'TM0404':'194.95.45.144',
		'TM0405':'194.95.45.145',
		'TM0406':'194.95.45.146',
		'TM0407':'194.95.45.147',
		'TM0408':'194.95.45.148',
		'TM0409':'194.95.45.149',
		'TM0410':'194.95.45.150',
		'TM0411':'194.95.45.151',
		'TM0412':'194.95.45.152',
		'TM0413':'194.95.45.153',
		'TM0414':'194.95.45.154',
		'TM0415':'194.95.45.155',
		'TM0416':'194.95.45.156',
		'localhost':'127.0.0.1'
	}

	name_value_separator = '='

	host_status = []
	lines = []

	service_status_found = False
	host_status_found = False

	def __init__(self):
		pass

	# parses the status.dat file, which by default is located at
	# /usr/local/nagios/var/status.dat
	def parse(self, file_path='/usr/local/nagios/var/status.dat'):
		del self.host_status[:]
		del self.lines[:]

		host_counter = 0
		
		with open(file_path, 'r') as status_file:

			# iterate over all lines of the status.dat file
			for line in status_file:
				stripped_line = line.strip('\t\n')

				# if a block of service information starts
				if self.contains(stripped_line, self.SERVICE_START):
					self.service_status_found = True
					del self.lines[:]

				# if the end of the service information block is found
				elif self.service_status_found and self.contains(stripped_line, '}'):
					self.parse_service_status(self.lines) # begin parsing a service status
					self.service_status_found = False
					del self.lines[:]

				# if one line of the service information is found append the line
				elif self.service_status_found:
					self.lines.append(stripped_line)

				# if a hoststatus beginning is found
				if self.contains(stripped_line, self.HOST_START):
					self.host_status_found = True
					del self.lines[:]

				# if the end of a hoststatus block is found
				elif self.host_status_found and self.contains(stripped_line, '}'):
					host_counter += 1
					self.parse_host_status(self.lines) # begin parsing a service status
					self.host_status_found = False
					del self.lines[:]

				# if a lin of the hoststatus block is found
				elif self.host_status_found:
					self.lines.append(stripped_line)

		return self.host_status

	def parse_service_status(self, lines):
		service_description = 'unknown'
		host_name = 'unknown'
		host_index = -1
		
		for line in lines:
			if self.contains(line, 'host_name'):
				host_name = self.get_value_from_line(line)
				host_index = self.get_host_index(host_name)
				
				#print('===================='+host_name+'====================')
			
			elif self.contains(line, 'service_description'):
				service_description = self.get_value_from_line(line)
			
			elif self.contains(line, 'plugin_output') and not self.contains(line, 'long_plugin_output'):
				plugin_output = line[line.find('=')+1:]
				
				resource_status = self.get_resource_status(line, service_description, plugin_output)

				# add gathered status information to host status data
				if len(resource_status) > 0:
					for resource_key in resource_status:
						self.host_status[host_index]['resource'][resource_key] = resource_status[resource_key]
			

	def parse_host_status(self, lines):
		is_online = False

		latency = -1
		loss_percent = -1
		status = 'UNKNOWN'



		for line in lines:
			if self.contains(line, 'host_name'):
				host_name = self.get_value_from_line(line)
				print('found hoststatus of host: ' + host_name)
				
			elif (
				self.contains(line, 'plugin_output') and
				not self.contains(line, 'long_plugin_output')
			):
				ping_status = self.get_value_from_line(line)
				print('ping_status of found hoststatus:' + ping_status)
				is_online = self.contains(ping_status, 'PING OK')

				#PING OK - Packet loss = 0%, RTA = 0.09 ms
				if(ping_status.find('Host Unreachable') != -1):
					status = 'Unreachable'
					loss_percent = 100
					latency = -1
				else:
					status = re.findall('[[A-Z]*\s[A-Z]*]*', ping_status)[0]
					loss_percent = re.findall('\d+%', ping_status)[0]
					loss_percent = re.findall('\d+', loss_percent)[0]
					latency = re.findall('\d+\s*ms', ping_status)[0]
					latency = re.findall('\d+', latency)[0]
					
					print('HOST STATUS: ' + status)
					print('LOSS PERCENT: ' + loss_percent)
					print('LATENCY: ' + latency)

		ip = self.ips.get(host_name, 'unknown')

		# add all found attributes to the hosts dictionary of status information
		self.host_status.append({
			'identity': {
				'host_name':host_name,
				'online':is_online,
				'ip':ip
			}, 'resource': {
				"network": {
					"latency": latency,
					"loss_percent": loss_percent,
					"status": status
        		}
			}
		})

	def contains(self, text, substring):
		return text.find(substring) != -1
	
	
	def get_value_from_line (self, line):
		return line.split(sep=self.name_value_separator, maxsplit=1)[1]
	
	def get_host_index (self, host_name):
		result = -1
		for i in range(len(self.host_status)):
			if self.host_status[i]['identity']['host_name'] == host_name:
				result = i
				break
		return result
	
	def get_resource_status(self, line, service_description, plugin_output):
		result = {}
		result.clear()
		
		"""
		service_description_to_resource_name = {
		'CPU Load':'cpu load', # e.g.: CHECK_NRPE: Socket timeout after 10 seconds. / OK - load average: 0.16, 0.11, 0.12
		'Current Load':'unkown', # e.g.: OK - load average: 0.47, 0.37, 0.33
		'Current Users':'unknown', # e.g.: USERS OK - 5 users currently logged in
		'HTTP':'unknown', # e.g.: HTTP WARNING: HTTP/1.1 403 Forbidden - 1249 bytes in 0.003 second response time
		'PING':'unknown', # e.g.: PING OK - Packet loss = 0%, RTA = 0.10 ms
		'Root Partition':'unknown', # e.g.: DISK OK - free space: / 31408 MB (78% inode=-):
		'SSH':'unknown', # e.g.: connect to address 127.0.0.1 and port 22: Connection refused
		'Swap Usage':'swap usage', # e.g.: SWAP OK - 100% free (2052 MB out of 2060 MB)
		'Total Processes':'unknown' # e.g.: PROCS OK: 72 processes with STATE = RSZDT
	}
		"""
		
		#print('plugin_output:>>>'+plugin_output+'<<<\n')
		
		if service_description == 'CPU Load':
			result = gather_status_data_from_cpu_load(plugin_output)
			
		elif service_description == 'Current Load':
			pass

		# local users
		elif service_description == 'Current Users':
			result = gather_status_data_from_current_users(plugin_output)

		# remote users
		elif service_description == 'Users':
			result = gather_status_data_from_remote_users(plugin_output)

		elif service_description == 'HTTP':
			pass

		elif service_description == 'PING':
			result = gather_status_data_from_ping(plugin_output)
		
		# local disk
		elif service_description == 'Root Partition':
			result = gather_status_data_from_check_local_disk(plugin_output)
			
		# remote disk
		elif service_description == 'Disk Space on sda':
			result = gather_status_data_from_remote_disk(plugin_output)

		elif service_description == 'SSH':
			pass
		elif service_description == 'Swap Usage':
			pass
		elif service_description == 'Total Processes':
			pass
		
		
		
		return result
	
	def print_result_dict(self, dictionary):
		for i in range(len(dictionary)):
			for key in dictionary[i]:
				print(key + ':' + str(dictionary[i][key]))
			print('\n')
		