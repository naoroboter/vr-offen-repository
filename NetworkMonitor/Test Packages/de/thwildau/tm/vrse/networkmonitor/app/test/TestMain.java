/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.app.test;

import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import de.thwildau.tm.vrse.networkmonitor.app.Main;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christian
 */
public class TestMain {
    
    private static Main app;
    
    public TestMain() {
    }
    
    @BeforeClass
    public static void setUpClass() {
         app = new Main();
       //Yeah, load the defaults...
        AppSettings settings = new AppSettings(true);
        //We are not going to use any input, that should be cover by our game 
        //testers they know how to do it, is their job :).
        settings.setUseInput(false);
        app.setSettings(settings);
        //This will run in our jenkins server so, no monitor, no display :).
        app.start(JmeContext.Type.Headless);
        //This method normally is called in the GL/Rendering thread so any 
        //GL-dependent resources can be initialized.
        //We don't have any display in our jenkins server so it is fine 
        //to calling manually here...
       //  app.initialize();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       
    }
    
    @After
    public void tearDown() {
    }
  
    @Test
    public void testInitLight() {
       
    }
        
}