package de.thwildau.tm.vrse.networkmonitor.app;
 
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Spatial;
import com.jme3.scene.Node;
import de.thwildau.tm.vrse.networkmonitor.player.ThirdPersonPlayerNode;
 
/**
 * Main-Class to create the VR-World and the Player
 * @author TM14
 */
public class Main extends SimpleApplication
{    
    private Spatial sceneModel;
    private RigidBodyControl scene;
    private BulletAppState bulletAppState;
    private ThirdPersonPlayerNode player;
 
    
    /**
     * Start-Method to create the BulletAppState to save 
     * the physic state of the player and the world. 
     */
    @Override
    public void simpleInitApp() { 
        System.out.println("Starte SimpleInitApp");
        mouseInput.setCursorVisible(false);
	flyCam.setEnabled(false);
 
	bulletAppState = new BulletAppState();
	stateManager.attach(bulletAppState);
 
        initLight();
        initSky();
        initWorld();
        initPlayer();
    }
 
    
    /**
     * Method to create a simple Ambient-Light
     */
    public void initLight(){
        // **add light
        AmbientLight light = new AmbientLight();
        light.setColor(ColorRGBA.White.mult(2));
        rootNode.addLight(light);
    }
    
    
    /**
     * Method to create a Sky-Box in the VR-World
     */
     public void initSky(){
        Spatial sky = assetManager.loadModel("/Models/Sky/Sky.j3o");
        rootNode.attachChild(sky);
    }
    
     
     /**
      * Method to load the Model and set it into a Collision-Shape to detect Objects
      * that collision with the SceneModel (the House in the World).
      * To add this Collision-Shape to the Model it first must be set into
      * a RigidBodyControl-Object.
      * At last the Child is added to the rootNode of the Scene and
      * activate the Physics for the scene.
      * 
      */
     public void initWorld(){
        sceneModel = assetManager.loadModel("/Models/mainHouse/hausnew.j3o");
	CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) sceneModel);
        scene = new RigidBodyControl(sceneShape, 0);
	sceneModel.addControl(scene);
        
        rootNode.attachChild(sceneModel);
	bulletAppState.getPhysicsSpace().add(scene);
     }
     
     
     /**
      * Method to load a Player-Model and create a ThirdPersonPlayerNode to
      * detect the players in a Multiplayer-Game.
      * The ThirdPersonPlayerNode contains the model itself, an inputmanager
      * to detect Keys and the cam to follow the player.
      * Last the Player is added to the rootNode and the Physics will be intialize.
      */
    public void initPlayer(){
        Spatial playerModel = assetManager.loadModel("/Models/Ninja/Ninja.mesh.j3o");
	player = new ThirdPersonPlayerNode(playerModel, inputManager, cam);
	rootNode.attachChild(player);
	bulletAppState.getPhysicsSpace().add(player);
    }
     
    
    /**
     * This Method is called everytime the player moves.
     * The Position of the player is update by the update()-Method of the 
     * ThirdPersonPlayerNode-Object.
     * @param tpf 
     */
    @Override
    public void simpleUpdate(float tpf)
    {
	player.update();
    }
 
    
    @Override
    public void simpleRender(RenderManager rm)
    {
        //TODO: add render code
    }
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }
}

