
package de.thwildau.tm.vrse.networkmonitor.player;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.HttpZipLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.ChaseCamera;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.material.MaterialList;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.control.CameraControl.ControlDirection;
import com.jme3.scene.plugins.ogre.OgreMeshKey;

/**
 *
 * @author BKonstantin
 */
public class WalkingCharacterDemo extends SimpleApplication implements ActionListener, AnimEventListener {
 
    public static void main(String[] args) {
        WalkingCharacterDemo app = new WalkingCharacterDemo();
        app.start();
    }
    
    private BulletAppState bulletAppState;
    private Node gameLevel;
    private CharacterControl character;
    private Node model;
    private AnimChannel animationChannel;
    private AnimChannel attackChannel;
    private AnimControl animationControl;
    private ChaseCamera chaseCam;
    // ***walk
    private Vector3f walkDirection = new Vector3f(0,0,0); // stop
    private float airTime = 0;
    
    // track directional input, so we can walk left-forward etc
    private boolean left = false, right = false, up = false, down = false;
    private CameraNode camNode;

    public void simpleInitApp() { 
        
        // ***Activate physics in the scene by adding a BulletAppState.
        bulletAppState = new BulletAppState();
        //bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        stateManager.attach(bulletAppState);
        // ***Initialize the Scene #1
//        PhysicsTestHelper.createPhysicsTestWorld(rootNode,
//        assetManager, bulletAppState.getPhysicsSpace());
        // ***set move speed
        //flyCam.setMoveSpeed(30);

          // ***Initialize the Scene #2
        //assetManager.registerLocator("quake3level.zip", ZipLocator.class);
//        assetManager.registerLocator("http://jmonkeyengine.googlecode.com/files/quake3level.zip",HttpZipLocator.class);
//        MaterialList matList = (MaterialList) assetManager.loadAsset("Scene.material");
//        OgreMeshKey key = new OgreMeshKey("main.meshxml", matList);
//        gameLevel = (Node) assetManager.loadAsset(key);
        
        // ***define the model "hausnew.j3o" as game level
        gameLevel= (Node) assetManager.loadModel("Models/mainHouse/hausnew.j3o"); 
        //gameLevel.setLocalTranslation(-20, -16, 20);
        //gameLevel.setLocalScale(0.3f);
        gameLevel.addControl(new RigidBodyControl(0));
        Node receptionPCs = (Node) assetManager.loadModel("Models/receptionPCs/newRec.j3o"); 
        gameLevel.attachChild(receptionPCs);
        rootNode.attachChild(gameLevel);
        bulletAppState.getPhysicsSpace().addAll(gameLevel);
        
        // ***add light
        AmbientLight light = new AmbientLight();
        light.setColor(ColorRGBA.White.mult(2));
        rootNode.addLight(light);
        
        // ***Create the Animated Character
         CapsuleCollisionShape capsule = new CapsuleCollisionShape(0.4f, 0.5f); // 3f,4f
        character = new CharacterControl(capsule, 0.5f);
        character.setJumpSpeed(10f);
        model = (Node) assetManager.loadModel("Models/player2/char2.j3o");
        //model = (Node) assetManager.loadModel("Models/Oto/Oto.mesh.j3o");
        //model.setLocalScale(0.3f);
        model.addControl(character);
        
        bulletAppState.getPhysicsSpace().add(character);
        rootNode.attachChild(model);
        // ***Set Up AnimControl and AnimChannels
//        animationControl = model.getControl(AnimControl.class);
//        animationControl.addListener(this);
//        animationChannel = animationControl.createChannel();
//        attackChannel = animationControl.createChannel();
//        attackChannel.addBone(animationControl.getSkeleton().getBone("uparm.right"));
//        attackChannel.addBone(animationControl.getSkeleton().getBone("arm.right"));
//        attackChannel.addBone(animationControl.getSkeleton().getBone("hand.right"));
        
        // **Add ChaseCam / CameraNode)
        // Disable the default flyby cam
//        flyCam.setEnabled(false);
//        //create the camera Node
//        camNode = new CameraNode("Camera Node", cam);
//        //This mode means that camera copies the movements of the target:
//        camNode.setControlDir(ControlDirection.SpatialToCamera);
//        //Attach the camNode to the target:
//        model.attachChild(camNode);
//        //Move camNode, e.g. behind and above the target:
//        camNode.setLocalTranslation(new Vector3f(0, 5, -5));
//        //Rotate the camNode to look at the target:
//        camNode.lookAt(model.getLocalTranslation(), Vector3f.UNIT_Y);
        
        // Disable the default flyby cam
        flyCam.setEnabled(false);
        // Enable a chase cam for this target (typically the player).
        ChaseCamera chaseCam = new ChaseCamera(cam, model, inputManager);
        chaseCam.setSmoothMotion(true);
        
        // configure mappings, e.g. the WASD keys
        inputManager.addMapping("CharLeft", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("CharRight", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("CharForward", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("CharBackward", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("CharJump", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addMapping("CharAttack", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(this, "CharLeft", "CharRight");
        inputManager.addListener(this, "CharForward", "CharBackward");
        inputManager.addListener(this, "CharJump", "CharAttack");
    
    }

    @Override
    public void simpleUpdate(float tpf) { 
        Vector3f camDir = cam.getDirection().clone();
        Vector3f camLeft = cam.getLeft().clone();
        camDir.y = 0;
        camLeft.y = 0;
        camDir.normalizeLocal();
        camLeft.normalizeLocal();
        walkDirection.set(0, 0, 0);

        if (left)  walkDirection.addLocal(camLeft);
        if (right) walkDirection.addLocal(camLeft.negate());
        if (up) walkDirection.addLocal(camDir);
        if (down) walkDirection.addLocal(camDir.negate());

        if (!character.onGround()) { // use !character.isOnGround() if the character is a BetterCharacterControl type.
            airTime += tpf;
        } else {
            airTime = 0;
        }

//        if (walkDirection.lengthSquared() == 0) { //Use lengthSquared() (No need for an extra sqrt())
//            if (!"stand".equals(animationChannel.getAnimationName())) {
//              animationChannel.setAnim("stand", 1f);
//            }
//        } else {
//            character.setViewDirection(walkDirection);
//            if (airTime > .3f) {
//              if (!"stand".equals(animationChannel.getAnimationName())) {
//                animationChannel.setAnim("stand");
//              }
//            } else if (!"Walk".equals(animationChannel.getAnimationName())) {
//              animationChannel.setAnim("Walk", 0.7f);
//            }
//          }

        walkDirection.multLocal(2f).multLocal(tpf);// The use of the first multLocal here is to control the rate of movement multiplier for character walk speed. The second one is to make sure the character walks the same speed no matter what the frame rate is.
        character.setWalkDirection(walkDirection); // THIS IS WHERE THE WALKING HAPPENS
    
    }

    public void onAction(String binding, boolean value, float tpf) {
        
        if (binding.equals("CharLeft")) {
            if (value) left = true;
            else left = false;
        } else if (binding.equals("CharRight")) {
            if (value) right = true;
            else right = false;
        } else if (binding.equals("CharForward")) {
            if (value) up = true;
            else up = false;
        } else if (binding.equals("CharBackward")) {
            if (value) down = true;
            else down = false;
        } else if (binding.equals("CharJump"))
            character.jump();
        if (binding.equals("CharAttack"))
          attack();
    
    }
    
    private void attack() {
        attackChannel.setAnim("Dodge", 0.1f);
        attackChannel.setLoopMode(LoopMode.DontLoop);
    }

      public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
          if (channel == attackChannel) channel.setAnim("stand");       
      }

      public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
      
      }
}