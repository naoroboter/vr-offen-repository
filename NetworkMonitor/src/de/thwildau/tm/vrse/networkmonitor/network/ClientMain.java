package de.thwildau.tm.vrse.networkmonitor.network;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.Network;
import com.jme3.network.serializing.Serializer;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import com.jme3.network.ClientStateListener;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import de.thwildau.tm.vrse.networkmonitor.player.ThirdPersonPlayerNode;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.openal.AL;

/**
 *
 * @author BKonstantin
 */
public class ClientMain extends SimpleApplication implements ClientStateListener {
    
    Client myClient;
    Node mainOffice;
    Spatial playerModel,officePCs,adminPC,server1,server2,basementWalls,receptionPCs;
    private BulletAppState bulletAppState;
    private ThirdPersonPlayerNode player;
    private RigidBodyControl scene;
    
  public static void main(String[] args) {
      // <start app>
      ClientMain app = new ClientMain();
            // <set FPS>
            AppSettings frameRate = new AppSettings(true);
            frameRate.setFrameRate(30); // 30 FPS
            app.setSettings(frameRate);
            // </ set FPS>   
       app.start(JmeContext.Type.Display); // standard display type
       // </ start app>
  }


    @Override
    public void simpleInitApp() {
        mouseInput.setCursorVisible(false);
	flyCam.setEnabled(false); // switch off first view camera 
 
	bulletAppState = new BulletAppState();
	stateManager.attach(bulletAppState);
 
        addModels(); // add all models
        
        // ***add player (character)
        playerModel = assetManager.loadModel("/Models/player2/char2.j3o");
	player = new ThirdPersonPlayerNode(playerModel, inputManager, cam);
	rootNode.attachChild(player);
	bulletAppState.getPhysicsSpace().add(player);
        
        initLight(); // init light
        
        //<create client>        
      try {
          myClient = Network.connectToServer("localhost", 6143);
          myClient.start();
          // register the ClientStateListener interface 
          myClient.addClientStateListener(this);
          // register each message type to Serializer
          Serializer.registerClass(HelloMessage.class);
          //  register a client listener (for each message type)
          myClient.addMessageListener(new ClientListener(), HelloMessage.class);
          System.out.println("Client is connected!!!\n");

      } catch (IOException ex) {
          System.err.println("There is no connection to the server!");
          //Logger.getLogger(ClientMain.class.getName()).log(Level.SEVERE, null, ex);
      }
        //</ create client>        
    }
    
    private void addModels(){
        
         // alle j3o-Binary hinzufuegen
        mainOffice = (Node) assetManager.loadModel("/Models/mainHouse/hausnew.j3o");
        officePCs = assetManager.loadModel("/Models/officePCs/allPcsandTables.j3o"); 
        mainOffice.attachChild(officePCs);
        adminPC = assetManager.loadModel("/Models/adminPC/adminpc.j3o"); 
        mainOffice.attachChild(adminPC);
        server1 = assetManager.loadModel("/Models/server1/server1.j3o"); 
        mainOffice.attachChild(server1);
        server2 = assetManager.loadModel("/Models/server2/server2.j3o"); 
        mainOffice.attachChild(server2);
        basementWalls = assetManager.loadModel("/Models/basementWalls/kellerwand.j3o"); 
        mainOffice.attachChild(basementWalls);
        receptionPCs = assetManager.loadModel("/Models/receptionPCs/newRec.j3o"); 
        mainOffice.attachChild(receptionPCs);
        
        // make whole mainOffice as CollisionShapeFactory
        // so that the player can not walk through walls etc.
        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) mainOffice);
	scene = new RigidBodyControl(sceneShape, 0);
	mainOffice.addControl(scene);
        rootNode.attachChild(mainOffice);
	bulletAppState.getPhysicsSpace().add(scene);
    }
    /**
     * light initialization
     */
    public void initLight(){
//        DirectionalLight sun = new DirectionalLight();
//        sun.setColor(ColorRGBA.White);
//        sun.setDirection(new Vector3f(.5f,.5f,.5f).normalizeLocal());
//        rootNode.addLight(sun);    
        // ***add AmbientLight
        AmbientLight light = new AmbientLight();
        light.setColor(ColorRGBA.White.mult(2));
        rootNode.addLight(light);
    }
    
        
    /**
     * Sending message to the server
     * @param myClient 
     */
    private void sendMessage(){
        Message message = new HelloMessage("Hello Server!");
        myClient.send(message);
    
    }
    
    @Override
    public void destroy(){
        myClient.close();
        super.destroy();
    }

    /**
     * The method is called for each client 
     * when the connection to the server was successful
     * @param c 
     */
    public void clientConnected(Client c) {
        // send message to the server
        sendMessage();
    }

    public void clientDisconnected(Client c, DisconnectInfo info) {
        System.out.println("The server kicked this client :(");
        c.close();
        destroy();      
    }
    
    @Override
    public void simpleUpdate(float tpf)
    {
	player.update();
    }
 
    @Override
    public void simpleRender(RenderManager rm)
    {
        //TODO: add render code
    }
}