package de.thwildau.tm.vrse.networkmonitor.network;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author BKonstantin
 */
    // Creating Message Types
@Serializable
public class HelloMessage extends AbstractMessage {
  private String hello;       // custom message data
  
  public HelloMessage() {}    // empty constructor
  
  public HelloMessage(String s) { hello = s; } // custom constructor

    String getMessage() {
       return hello;
    }
}
