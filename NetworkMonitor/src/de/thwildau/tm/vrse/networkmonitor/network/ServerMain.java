package de.thwildau.tm.vrse.networkmonitor.network;

import com.jme3.app.SimpleApplication;
import com.jme3.network.Message;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.network.serializing.Serializer;
import com.jme3.system.JmeContext;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BKonstantin
 */
public class ServerMain extends SimpleApplication implements ConnectionListener {
    
     Server myServer;
     
    public static void main(String[] args) {
      ServerMain app = new ServerMain();
      app.start(JmeContext.Type.Headless); // headless type for servers!
    }

    @Override
    public void simpleInitApp() {       
        // < create server>
      try {
            myServer = Network.createServer(6143);// Port: 6143
            myServer.start();
            // register the ConnectionListener interface 
            myServer.addConnectionListener(this);
            // register each message type to Serializer
            Serializer.registerClass(HelloMessage.class);
            // register a server listener (for each message type)
            myServer.addMessageListener(new ServerListener(), HelloMessage.class);
            System.out.println("Server is started!!!\n");
            
      } catch (IOException ex) {
          Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
      }
      // </ create server>                    
    }
    
    /**
     * Sending message to all clients (broadcast)
     * @param myServer 
     */
    private void sendMessageToAllClients(){
        Message message = new HelloMessage("Welcome!");
        myServer.broadcast(message);
    
    }
    
    @Override
    public void destroy() {
        myServer.close();
        super.destroy();
    }
    /**
     * The method is only called when the connection of the client was successful
     * @param server
     * @param conn 
     */
    public void connectionAdded(Server server, HostedConnection conn) {
        System.out.println("Number of connected clients: "+myServer.getConnections().size());
        // send message to all clients
        sendMessageToAllClients();
    }

    public void connectionRemoved(Server server, HostedConnection conn) {
        System.out.println("Client #"+conn.getId()+" has quit the game.");
        
    }
    
}
