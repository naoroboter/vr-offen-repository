/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import com.jme3.app.Application;
import com.jme3.ui.Picture;
import networkMonitoring.AMain.ClientMain;
import networkMonitoring.AppStates.WorldState;

/**
 * Ideas: - give statemanager as a parameter into the constructor - worldstate
 * --> singleton - find a way to get the state manager
 *
 * @author xiaolong
 */
public class MonitoringGraphDrawer {

	private ClientMain app;
	private WorldState worldState;

	private Picture cpuUsageChart;
	private Picture diskUsageChart;
	private Picture pingChart;
	private Picture userCountChart;
	
	private boolean chartIsShown = false;
	
	public MonitoringGraphDrawer(Application app) {
		this.app = (ClientMain) app;
		this.worldState = app.getStateManager().getState(WorldState.class);
	}

	public void drawMonitoringGraph(MonitoredHost monitoredHost) {
		
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		
		cpuUsageChart = new Picture("Monitoring_CPUUsageChart");
		pingChart = new Picture("Monitoring_PingChart");
		diskUsageChart = new Picture("Monitoring_DiskUsageChart");
		userCountChart = new Picture("Monitoring_UserCountChart");
		
		// load the charts
		cpuUsageChart.setImage(app.getAssetManager(), "Textures/Charts/cpu-usage-"+monitoredHost.toString()+".PNG", true);
		diskUsageChart.setImage(app.getAssetManager(), "Textures/Charts/disk-usage-"+monitoredHost.toString()+".PNG", true);
		pingChart.setImage(app.getAssetManager(), "Textures/Charts/ping-"+monitoredHost.toString()+".PNG", true);
		userCountChart.setImage(app.getAssetManager(), "Textures/Charts/user-count-"+monitoredHost.toString()+".PNG", true);
		
		// set dimensions for charts
		cpuUsageChart.setWidth(app.getSettings().getWidth()/2);
		cpuUsageChart.setHeight(app.getSettings().getHeight()/2);
		
		diskUsageChart.setWidth(app.getSettings().getWidth()/2);
		diskUsageChart.setHeight(app.getSettings().getHeight()/2);
		
		pingChart.setWidth(app.getSettings().getWidth()/2);
		pingChart.setHeight(app.getSettings().getHeight()/2);
		
		userCountChart.setWidth(app.getSettings().getWidth()/2);
		userCountChart.setHeight(app.getSettings().getHeight()/2);
		
		// set positions for charts
		float cpuUsageChartRow = app.getSettings().getHeight()/2;
		float cpuUsageChartCol = 0;
		cpuUsageChart.setPosition(cpuUsageChartCol, cpuUsageChartRow);
		
		float pingChartRow = app.getSettings().getHeight()/2;
		float pingChartCol = app.getSettings().getWidth()/2;
		pingChart.setPosition(pingChartCol, pingChartRow);
		
		float diskUsageChartRow = 0;
		float diskUsageChartCol = app.getSettings().getWidth()/2;
		diskUsageChart.setPosition(diskUsageChartCol, diskUsageChartRow);
		
		float userCountChartRow = 0;
		float userCountChartCol = 0;
		userCountChart.setPosition(userCountChartCol, userCountChartRow);
		
		// attach images to GUI
		app.getGuiNode().attachChild(cpuUsageChart);
		app.getGuiNode().attachChild(pingChart);
		app.getGuiNode().attachChild(diskUsageChart);
		app.getGuiNode().attachChild(userCountChart);
	}
	
	public void hideMonitoringGraph() {
		try {
			app.getGuiNode().detachChild(cpuUsageChart);
			app.getGuiNode().detachChild(pingChart);
			app.getGuiNode().detachChild(diskUsageChart);
			app.getGuiNode().detachChild(userCountChart);
		} catch(Exception ex) {
			// do nothing
		}
	}
	
	public void toggleMonitoringGraph(MonitoredHost monitoredHost) {
		if(chartIsShown) {
			hideMonitoringGraph();
		} else {
			if(monitoredHost != null) {
				drawMonitoringGraph(monitoredHost);
			}
		}
		chartIsShown = !chartIsShown;
	}
	
	public boolean isChartShown() {
		return chartIsShown;
	}
}
