/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

/**
 *
 * @author xiaolong
 */
public enum MonitoredHost {
	
	LOCALHOST("localhost"),
	TM0401("TM0401"),
	TM0402("TM0402"),
	TM0403("TM0403"),
	TM0404("TM0404"),
	TM0405("TM0405"),
	TM0406("TM0406"),
	TM0407("TM0407"),
	TM0408("TM0408"),
	TM0409("TM0409"),
	TM0410("TM0410"),
	TM0411("TM0411"),
	TM0412("TM0412"),
	TM0413("TM0413"),
	TM0414("TM0414"),
	TM0415("TM0415"),
	TM0416("TM0416");
	
	private final String text;

	private MonitoredHost(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}