package networkMonitoring.Animation;

import com.jme3.animation.LoopMode;
import com.jme3.asset.AssetManager;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Spline;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BKonstantin
 */
public class PackageAnimation {
    
    private Spatial b;
    private boolean active = true;
    private boolean playing = false;
    private MotionPath path;
    private MotionEvent motionControl;
    Node rootNode;
    AssetManager assetManager;
    InputManager inputManager;
    
        private MotionPath path1; private MotionEvent motionControl1;
    private MotionPath path2; private MotionEvent motionControl2;
    private MotionPath path3; private MotionEvent motionControl3;
    private MotionPath path4; private MotionEvent motionControl4;
    private MotionPath path5; private MotionEvent motionControl5;
    private MotionPath path6; private MotionEvent motionControl6;
    private MotionPath path7; private MotionEvent motionControl7;
    private MotionPath path8; private MotionEvent motionControl8;
    private MotionPath path9; private MotionEvent motionControl9;
    private MotionPath path10; private MotionEvent motionControl10;
    private MotionPath path11; private MotionEvent motionControl11;
    private MotionPath path12; private MotionEvent motionControl12;
    private MotionPath path13; private MotionEvent motionControl13;
    private MotionPath path14; private MotionEvent motionControl14;
    private MotionPath path15; private MotionEvent motionControl15;
    private MotionPath path16; private MotionEvent motionControl16;
    private MotionPath path17; private MotionEvent motionControl17;
    private MotionPath path18; private MotionEvent motionControl18;
    private MotionPath path19; private MotionEvent motionControl19;
    private MotionPath path20; private MotionEvent motionControl20;
    private MotionPath path21; private MotionEvent motionControl21;
    private MotionPath path22; private MotionEvent motionControl22;
    private MotionPath path23; private MotionEvent motionControl23;
    private MotionPath path24; private MotionEvent motionControl24;
    
    
    public PackageAnimation(Node rootNode,AssetManager assetManager, InputManager inputManager){
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.inputManager = inputManager;       
        startAnimation();
    }
    
    public void startAnimation(){
        
        Sphere s1 = new Sphere(16, 16, 0.2f);
        Box b1 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom1 = new Geometry("Box", s1);
        Material mat1 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat1.setColor("Color", ColorRGBA.Blue);
        geom1.setMaterial(mat1);
        rootNode.attachChild(geom1);
        
        Sphere s2 = new Sphere(16, 16, 0.2f);
        Box b2 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom2 = new Geometry("Box", s2);
        Material mat2 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat2.setColor("Color", ColorRGBA.Blue);
        geom2.setMaterial(mat2);
        rootNode.attachChild(geom2);
        
        Sphere s3 = new Sphere(16, 16, 0.2f);
        Box b3 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom3 = new Geometry("Box", s3);
        Material mat3 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat3.setColor("Color", ColorRGBA.Blue);
        geom3.setMaterial(mat3);
        rootNode.attachChild(geom3);
        
        Sphere s4 = new Sphere(16, 16, 0.2f);
        Box b4 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom4 = new Geometry("Box", s4);
        Material mat4 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat4.setColor("Color", ColorRGBA.Blue);
        geom4.setMaterial(mat4);
        rootNode.attachChild(geom4);
        
        Sphere s5 = new Sphere(16, 16, 0.2f);
        Box b5 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom5 = new Geometry("Box", s5);
        Material mat5 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat5.setColor("Color", ColorRGBA.Blue);
        geom5.setMaterial(mat5);
        rootNode.attachChild(geom5);
        
        Sphere s6 = new Sphere(16, 16, 0.2f);
        Box b6 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom6 = new Geometry("Box", s6);
        Material mat6 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat6.setColor("Color", ColorRGBA.Blue);
        geom6.setMaterial(mat6);
        rootNode.attachChild(geom6);
        
        Sphere s7 = new Sphere(16, 16, 0.2f);
        Box b7 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom7 = new Geometry("Box", s7);
        Material mat7 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat7.setColor("Color", ColorRGBA.Blue);
        geom7.setMaterial(mat7);
        rootNode.attachChild(geom7);
        
        Sphere s8 = new Sphere(16, 16, 0.2f);
        Box b8 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom8 = new Geometry("Box", s8);
        Material mat8 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat8.setColor("Color", ColorRGBA.Blue);
        geom8.setMaterial(mat8);
        rootNode.attachChild(geom8);
        
        Sphere s9 = new Sphere(16, 16, 0.2f);
        Box b9 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom9 = new Geometry("Box", s9);
        Material mat9 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat9.setColor("Color", ColorRGBA.Blue);
        geom9.setMaterial(mat9);
        rootNode.attachChild(geom9);
        
        Sphere s10 = new Sphere(16, 16, 0.2f);
        Box b10 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom10 = new Geometry("Box", s10);
        Material mat10 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat10.setColor("Color", ColorRGBA.Blue);
        geom10.setMaterial(mat10);
        rootNode.attachChild(geom10);

        Sphere s11 = new Sphere(16, 16, 0.2f);
        Box b11 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom11 = new Geometry("Box", s11);
        Material mat11 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat11.setColor("Color", ColorRGBA.Blue);
        geom11.setMaterial(mat11);
        rootNode.attachChild(geom11);
        
        Sphere s12 = new Sphere(16, 16, 0.2f);
        Box b12 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom12 = new Geometry("Box", s12);
        Material mat12 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat12.setColor("Color", ColorRGBA.Blue);
        geom12.setMaterial(mat12);
        rootNode.attachChild(geom12);    
        
        Sphere s13 = new Sphere(16, 16, 0.2f);
        Box b13 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom13 = new Geometry("Box", s13);
        Material mat13 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat13.setColor("Color", ColorRGBA.Blue);
        geom13.setMaterial(mat13);
        rootNode.attachChild(geom13);  
        
        Sphere s14 = new Sphere(16, 16, 0.2f);
        Box b14 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom14 = new Geometry("Box", s14);
        Material mat14 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat14.setColor("Color", ColorRGBA.Blue);
        geom14.setMaterial(mat14);
        rootNode.attachChild(geom14);     
        
        Sphere s15 = new Sphere(16, 16, 0.2f);
        Box b15 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom15 = new Geometry("Box", s15);
        Material mat15 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat15.setColor("Color", ColorRGBA.Blue);
        geom15.setMaterial(mat15);
        rootNode.attachChild(geom15); 
        
        Sphere s16 = new Sphere(16, 16, 0.2f);
        Box b16 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom16 = new Geometry("Box", s16);
        Material mat16 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat16.setColor("Color", ColorRGBA.Blue);
        geom16.setMaterial(mat16);
        rootNode.attachChild(geom16);    
        
                Sphere s24 = new Sphere(16, 16, 0.2f);
        Box b24 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom24 = new Geometry("Box", s24);
        Material mat24 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat24.setColor("Color", ColorRGBA.Blue);
        geom24.setMaterial(mat24);
        rootNode.attachChild(geom24);

        Sphere s23 = new Sphere(16, 16, 0.2f);
        Box b23 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom23 = new Geometry("Box", s23);
        Material mat23 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat23.setColor("Color", ColorRGBA.Blue);
        geom23.setMaterial(mat23);
        rootNode.attachChild(geom23);

        Sphere s22 = new Sphere(16, 16, 0.2f);
        Box b22 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom22 = new Geometry("Box", s22);
        Material mat22 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat22.setColor("Color", ColorRGBA.Blue);
        geom22.setMaterial(mat22);
        rootNode.attachChild(geom22);

        Sphere s21 = new Sphere(16, 16, 0.2f);
        Box b21 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom21 = new Geometry("Box", s21);
        Material mat21 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat21.setColor("Color", ColorRGBA.Blue);
        geom21.setMaterial(mat21);
        rootNode.attachChild(geom21);

        Sphere s20 = new Sphere(16, 16, 0.2f);
        Box b20 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom20 = new Geometry("Box", s20);
        Material mat20 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat20.setColor("Color", ColorRGBA.Blue);
        geom20.setMaterial(mat20);
        rootNode.attachChild(geom20);

        Sphere s19 = new Sphere(16, 16, 0.2f);
        Box b19 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom19 = new Geometry("Box", s19);
        Material mat19 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat19.setColor("Color", ColorRGBA.Blue);
        geom19.setMaterial(mat19);
        rootNode.attachChild(geom19);

        Sphere s18 = new Sphere(16, 16, 0.2f);
        Box b18 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom18 = new Geometry("Box", s18);
        Material mat18 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat18.setColor("Color", ColorRGBA.Blue);
        geom18.setMaterial(mat18);
        rootNode.attachChild(geom18);

        Sphere s17 = new Sphere(16, 16, 0.2f);
        Box b17 = new Box(Vector3f.ZERO, 1, 1, 1);
        Geometry geom17 = new Geometry("Box", s17);
        Material mat17 = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat17.setColor("Color", ColorRGBA.Blue);
        geom17.setMaterial(mat17);
        rootNode.attachChild(geom17);
        
        
        
        
        //pc1st1 -> admin   
        path1 = new MotionPath();
  
        path1.addWayPoint(new Vector3f(
                (float) (46.4864-101.8), 
                (float) (2.5275385-2.2), 
                (float) (-139.7748 + 131.7)));
        
        path1.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        
        path1.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path1.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl1 = new MotionEvent(geom1, path1);
        motionControl1.setInitialDuration(8f);
        motionControl1.setSpeed(4f);       
        motionControl1.setLoopMode(LoopMode.Cycle);
        //motionControl1.play(); 
        //////////////////////////////////////////////////////////////////
        //pc2st1 -> admin   
        path2 = new MotionPath();
  
        path2.addWayPoint(new Vector3f(
                (float) (43.472607-101.8), 
                (float) (2.5311182-2.2), 
                (float) (-139.62592 + 131.7)));
        
        path2.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        
        path2.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path2.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl2 = new MotionEvent(geom2, path2);
        motionControl2.setInitialDuration(8f);
        motionControl2.setSpeed(4f);       
        motionControl2.setLoopMode(LoopMode.Cycle);
       // motionControl2.play(); 
        //////////////////////////////////////////////////////////////////        
         //pc3st1 -> admin   
        path3 = new MotionPath();
        //PC 
        path3.addWayPoint(new Vector3f(
                (float) (40.439114-101.8), 
                (float) (2.5311182-2.2), 
                (float) (-139.7056 + 131.7)));
        //server
        path3.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path3.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path3.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl3 = new MotionEvent(geom3, path3);
        motionControl3.setInitialDuration(8f);
        motionControl3.setSpeed(4f);       
        motionControl3.setLoopMode(LoopMode.Cycle);
       // motionControl3.play(); 
        //////////////////////////////////////////////////////////////////        
         //pc4st1 -> admin   
        path4 = new MotionPath();
        //PC 
        path4.addWayPoint(new Vector3f(
                (float) (37.002342-101.8), 
                (float) (2.651118-2.2), 
                (float) (-139.72469 + 131.7)));
        //server
        path4.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path4.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path4.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl4 = new MotionEvent(geom4, path4);
        motionControl4.setInitialDuration(8f);
        motionControl4.setSpeed(4f);       
        motionControl4.setLoopMode(LoopMode.Cycle);
       // motionControl4.play(); 
        //////////////////////////////////////////////////////////////////
        
         //pc5st1 -> admin   
        path5 = new MotionPath();
        //PC 
        path5.addWayPoint(new Vector3f(
                (float) (33.91359-101.8), 
                (float) (2.4475386-2.2), 
                (float) (-139.2973 + 131.7)));
        //server
        path5.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path5.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path5.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl5 = new MotionEvent(geom5, path5);
        motionControl5.setInitialDuration(8f);
        motionControl5.setSpeed(4f);       
        motionControl5.setLoopMode(LoopMode.Cycle);
        //motionControl5.play(); 
        //////////////////////////////////////////////////////////////////        
        
         //pc6st1 -> admin   
        path6 = new MotionPath();
        //PC 
        path6.addWayPoint(new Vector3f(
                (float) (34.09637-101.8), 
                (float) (2.4475384-2.2), 
                (float) (-136.11372 + 131.7)));
        //server
        path6.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path6.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path6.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl6 = new MotionEvent(geom6, path6);
        motionControl6.setInitialDuration(8f);
        motionControl6.setSpeed(4f);       
        motionControl6.setLoopMode(LoopMode.Cycle);
       // motionControl6.play(); 
        //////////////////////////////////////////////////////////////////          
        
         //pc7st1 -> admin   
        path7 = new MotionPath();
        //PC 
        path7.addWayPoint(new Vector3f(
                (float) (34.065857-101.8), 
                (float) (2.4475384-2.2), 
                (float) (-132.90007 + 131.7)));
        //server
        path7.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path7.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path7.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl7 = new MotionEvent(geom7, path7);
        motionControl7.setInitialDuration(8f);
        motionControl7.setSpeed(4f);       
        motionControl7.setLoopMode(LoopMode.Cycle);
       // motionControl7.play(); 
        //////////////////////////////////////////////////////////////////        
        
         //pc8st1 -> admin   
        path8 = new MotionPath();
        //PC 
        path8.addWayPoint(new Vector3f(
                (float) (34.09019-101.8), 
                (float) (2.4475384-2.2), 
                (float) (-129.58218 + 131.7)));
        //server
        path8.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path8.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path8.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl8 = new MotionEvent(geom8, path8);
        motionControl8.setInitialDuration(8f);
        motionControl8.setSpeed(4f);       
        motionControl8.setLoopMode(LoopMode.Cycle);
       // motionControl8.play(); 
        //////////////////////////////////////////////////////////////////   
        
         //pc1st2 -> admin   
        path9 = new MotionPath();
        //PC 
        path9.addWayPoint(new Vector3f(
                (float) (46.62225-101.8), 
                (float) (6.1781487-2.2), 
                (float) (-140.01675 + 131.7)));
        //server
        path9.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path9.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path9.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl9 = new MotionEvent(geom9, path9);
        motionControl9.setInitialDuration(8f);
        motionControl9.setSpeed(4f);       
        motionControl9.setLoopMode(LoopMode.Cycle);
       // motionControl9.play(); 
        //////////////////////////////////////////////////////////////////  
        
         //pc2st2 -> admin   
        path10 = new MotionPath();
        //PC 
        path10.addWayPoint(new Vector3f(
                (float) (43.608456-101.8), 
                (float) (6.1817284-2.2), 
                (float) (-139.86786 + 131.7)));
        //server
        path10.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path10.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path10.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl10 = new MotionEvent(geom10, path10);
        motionControl10.setInitialDuration(8f);
        motionControl10.setSpeed(4f);       
        motionControl10.setLoopMode(LoopMode.Cycle);
        motionControl10.play(); 
        ////////////////////////////////////////////////////////////////// 
        
         //pc3st2 -> admin   
        path11 = new MotionPath();
        //PC 
        path11.addWayPoint(new Vector3f(
                (float) (40.574963-101.8), 
                (float) (6.1817284-2.2), 
                (float) (-139.94754 + 131.7)));
        //server
        path11.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path11.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path11.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl11 = new MotionEvent(geom11, path11);
        motionControl11.setInitialDuration(8f);
        motionControl11.setSpeed(4f);       
        motionControl11.setLoopMode(LoopMode.Cycle);
       // motionControl11.play(); 
        //////////////////////////////////////////////////////////////////          
        
         //pc4st2 -> admin   
        path12 = new MotionPath();
        //PC 
        path12.addWayPoint(new Vector3f(
                (float) (37.13819-101.8), 
                (float) (6.3017282-2.2), 
                (float) (-139.96664 + 131.7)));
        //server
        path12.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path12.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path12.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl12 = new MotionEvent(geom12, path12);
        motionControl12.setInitialDuration(8f);
        motionControl12.setSpeed(4f);       
        motionControl12.setLoopMode(LoopMode.Cycle);
       // motionControl12.play(); 
        //////////////////////////////////////////////////////////////////  
        //pc5st2 -> admin   
        path13 = new MotionPath();
        //PC 
        path13.addWayPoint(new Vector3f(
              (float) (34.04944-101.8), 
                (float) (6.098149-2.2), 
                (float) (-139.53926 + 131.7)));
        //server
        path13.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path13.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path13.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl13 = new MotionEvent(geom13, path13);
        motionControl13.setInitialDuration(8f);
        motionControl13.setSpeed(4f);       
        motionControl13.setLoopMode(LoopMode.Cycle);
       // motionControl13.play(); 
        //////////////////////////////////////////////////////////////////        
        

//pc6st2 -> admin   
        path14 = new MotionPath();
        //PC 
        path14.addWayPoint(new Vector3f(
               (float) (34.23222-101.8), 
                (float) (6.098149-2.2), 
                (float) (-136.35567 + 131.7)));
        //server
        path14.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path14.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path14.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl14 = new MotionEvent(geom14, path14);
        motionControl14.setInitialDuration(8f);
        motionControl14.setSpeed(4f);       
        motionControl14.setLoopMode(LoopMode.Cycle);
       // motionControl14.play(); 
        //////////////////////////////////////////////////////////////////         
        
//pc7st2 -> admin   
        path15 = new MotionPath();
        //PC 
        path15.addWayPoint(new Vector3f(
               (float) (34.201706-101.8), 
                (float) (6.098149-2.2), 
                (float) (-133.14203 + 131.7)));
        //server
        path15.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path15.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path15.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl15 = new MotionEvent(geom15, path15);
        motionControl15.setInitialDuration(8f);
        motionControl15.setSpeed(4f);       
        motionControl15.setLoopMode(LoopMode.Loop);
        //motionControl15.play(); 
        //motionControl15.setDirection(Vector3f.ZERO);
        //////////////////////////////////////////////////////////////////         
        

//pc8st2 -> admin   
        path16 = new MotionPath();
        //PC 
        path16.addWayPoint(new Vector3f(
                (float) (34.22604-101.8), 
                (float) (6.098149-2.2), 
                (float) (-129.82414 + 131.7)));
        //server
        path16.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path16.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path16.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl16 = new MotionEvent(geom16, path16);
        motionControl16.setInitialDuration(8f);
        motionControl16.setSpeed(4f);       
        motionControl16.setLoopMode(LoopMode.Cycle);
       // motionControl16.play(); 
        //////////////////////////////////////////////////////////////////        

         //pc1st3 -> admin   
        path17 = new MotionPath();
        //PC 
        path17.addWayPoint(new Vector3f(
               (float) (47.018105-101.8), 
                (float) (9.942752-2.2), 
                (float) (-139.86691 + 131.7)));
        //server
        path17.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path17.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path17.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl17 = new MotionEvent(geom17, path17);
        motionControl17.setInitialDuration(8f);
        motionControl17.setSpeed(4f);       
        motionControl17.setLoopMode(LoopMode.Cycle);
       // motionControl17.play(); 
        ////////////////////////////////////////////////////////////////// 

      
//pc2st3 -> admin   
        path18 = new MotionPath();
        //PC 
        path18.addWayPoint(new Vector3f(
               (float) (44.00431-101.8), 
                (float) (9.946332-2.2), 
                (float) (-139.71803 + 131.7)));
        //server
        path18.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path18.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path18.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl18 = new MotionEvent(geom18, path18);
        motionControl18.setInitialDuration(8f);
        motionControl18.setSpeed(4f);       
        motionControl18.setLoopMode(LoopMode.Cycle);
       // motionControl18.play(); 
        ////////////////////////////////////////////////////////////////// 
//pc3st3 -> admin   
        path19 = new MotionPath();
        //PC 
        path19.addWayPoint(new Vector3f(
               (float) (40.97082-101.8), 
                (float) (9.946332-2.2), 
                (float) (-139.79771 + 131.7)));
        //server
        path19.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path19.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path19.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl19 = new MotionEvent(geom19, path19);
        motionControl19.setInitialDuration(8f);
        motionControl19.setSpeed(4f);       
        motionControl19.setLoopMode(LoopMode.Cycle);
       // motionControl19.play(); 
        ////////////////////////////////////////////////////////////////// 
//pc4st3 -> admin   
        path20 = new MotionPath();
        //PC 
        path20.addWayPoint(new Vector3f(
               (float) (37.53405-101.8), 
                (float) (10.066332-2.2), 
                (float) (-139.81682 + 131.7)));
        //server
        path20.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path20.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path20.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl20 = new MotionEvent(geom20, path20);
        motionControl20.setInitialDuration(8f);
        motionControl20.setSpeed(4f);       
        motionControl20.setLoopMode(LoopMode.Cycle);
       // motionControl20.play(); 
        ////////////////////////////////////////////////////////////////// 
//pc5st3 -> admin   
        path21 = new MotionPath();
        //PC 
        path21.addWayPoint(new Vector3f(
               (float) (34.445297-101.8), 
                (float) (9.862752-2.2), 
                (float) (-139.38942 + 131.7)));
        //server
        path21.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path21.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path21.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl21 = new MotionEvent(geom21, path21);
        motionControl21.setInitialDuration(8f);
        motionControl21.setSpeed(4f);       
        motionControl21.setLoopMode(LoopMode.Cycle);
       // motionControl21.play(); 
        ////////////////////////////////////////////////////////////////// 
//pc6st3 -> admin   
        path22 = new MotionPath();
        //PC 
        path22.addWayPoint(new Vector3f(
               (float) (34.62808-101.8), 
                (float) (9.862752-2.2), 
                (float) (-136.20584+ 131.7)));
        //server
        path22.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path22.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path22.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl22 = new MotionEvent(geom22, path22);
        motionControl22.setInitialDuration(8f);
        motionControl22.setSpeed(4f);       
        motionControl22.setLoopMode(LoopMode.Cycle);
       // motionControl22.play(); 
        ////////////////////////////////////////////////////////////////// 
//pc7st3 -> admin   
        path23 = new MotionPath();
        //PC 
        path23.addWayPoint(new Vector3f(
               (float) (34.597565-101.8), 
                (float) (9.862752-2.2), 
                (float) (-132.99219 + 131.7)));
        //server
        path23.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path23.addWayPoint(new Vector3f(
                (float) (41.169235-101.8), 
                (float) (-1.2413011-2.2), 
                (float) (-135.61803 + 131.7)));
        path23.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl23 = new MotionEvent(geom23, path23);
        motionControl23.setInitialDuration(8f);
        motionControl23.setSpeed(4f);       
        motionControl23.setLoopMode(LoopMode.Cycle);
       // motionControl23.play(); 
        ////////////////////////////////////////////////////////////////// 
//pc8st3 -> admin   
        path24 = new MotionPath();
        //PC 
        path24.addWayPoint(new Vector3f(
                (float) (34.621895-101.8), 
                (float) (9.862752-2.2), 
                (float) (-129.6743 + 131.7)));
        //server
        path24.addWayPoint(new Vector3f(
                -64.6f,
                -3.8f, 
                3f));
        //adminPC
        path24.addWayPoint(new Vector3f(
                (float) (34.621895-101.8), 
                (float) (9.862752-2.2), 
                (float) (-129.6743 + 131.7)));
        path24.enableDebugShape(assetManager, rootNode); //visible
        //path.disableDebugShape(); //invisible
        motionControl24 = new MotionEvent(geom24, path24);
        motionControl24.setInitialDuration(8f);
        motionControl24.setSpeed(4f);       
        motionControl24.setLoopMode(LoopMode.Cycle);
        //motionControl24.setDirection(Vector3f.);
       // motionControl24.play(); 
        //////////////////////////////////////////////////////////////////
        
          int randomInt = 0;
        Random randomGenerator = new Random();
        for (int idx = 1; idx <= 1000; ++idx){
            randomInt = randomGenerator.nextInt(25);
            System.out.println("Generated : " + randomInt);

        switch (randomInt) {
            case 1:  motionControl1.play(); 
                     break;
            case 2:  motionControl2.play(); 
                     break;
            case 3:  motionControl3.play(); 
                     break;
            case 4:  motionControl4.play(); 
                     break;
            case 5:  motionControl5.play(); 
                     break;
            case 6:  motionControl6.play(); 
                     break;
            case 7:  motionControl7.play(); 
                     break;
            case 8:  motionControl8.play(); 
                     break;
            case 9:  motionControl9.play(); 
                     break;
            case 10:  motionControl10.play(); 
                     break;
            case 11:  motionControl11.play(); 
                     break;
            case 12:  motionControl12.play(); 
                     break;
            case 13:  motionControl13.play(); 
                     break;
            case 14:  motionControl14.play(); 
                     break;
            case 15:  motionControl15.play(); 
                     break;
            case 16:  motionControl16.play(); 
                     break;           
            case 17:  motionControl17.play(); 
                     break;
            case 18:  motionControl18.play(); 
                     break;
            case 19:  motionControl19.play(); 
                     break;
            case 20:  motionControl20.play(); 
                     break;
            case 21:  motionControl21.play(); 
                     break;
            case 22:  motionControl22.play(); 
                     break;
            case 23:  motionControl23.play(); 
                     break;
            case 24:  motionControl24.play(); 
                     break;
            default: System.out.println("No Animation");
                     break;
        }   
            
        }
        
        
        
        
         initInputs();
    }
    
    private void initInputs() {
        inputManager.addMapping("display_hidePath", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("SwitchPathInterpolation", new KeyTrigger(KeyInput.KEY_I));
        inputManager.addMapping("tensionUp", new KeyTrigger(KeyInput.KEY_U));
        inputManager.addMapping("tensionDown", new KeyTrigger(KeyInput.KEY_J));
        inputManager.addMapping("play_stop", new KeyTrigger(KeyInput.KEY_SPACE));
        ActionListener acl = new ActionListener() {

            public void onAction(String name, boolean keyPressed, float tpf) {
                if (name.equals("display_hidePath") && keyPressed) {
                    if (active) {
                        active = false;
                        path.disableDebugShape();
                    } else {
                        active = true;
                        path.enableDebugShape(assetManager, rootNode);
                    }
                }
                if (name.equals("play_stop") && keyPressed) {
                    if (playing) {
                        playing = false;
                        motionControl1.stop();
                        motionControl2.stop();
                        motionControl3.stop();
                        motionControl4.stop();
                        motionControl5.stop();
                        motionControl6.stop();
                        motionControl7.stop();
                        motionControl8.stop();
                        motionControl9.stop();
                        motionControl10.stop();
                        motionControl12.stop();
                        motionControl11.stop();
                        motionControl13.stop();
                        motionControl14.stop();
                        motionControl15.stop();
                        motionControl16.stop();
                        motionControl17.stop();
                        motionControl18.stop();
                        motionControl19.stop();
                        motionControl20.stop();
                        motionControl21.stop();
                        motionControl22.stop();
                        motionControl23.stop();
                        motionControl24.stop();
                    } else {
                        playing = true;
                        motionControl1.play();
                        motionControl2.play();
                        motionControl3.play();
                        motionControl4.play();
                        motionControl5.play();
                        motionControl6.play();
                        motionControl7.play();
                        motionControl8.play();
                        motionControl9.play();
                        motionControl10.play();
                        motionControl12.play();
                        motionControl11.play();
                        motionControl13.play();
                        motionControl14.play();
                        motionControl15.play();
                        motionControl16.play();
                        motionControl17.play();
                        motionControl18.play();
                        motionControl19.play();
                        motionControl20.play();
                        motionControl21.play();
                        motionControl22.play();
                        motionControl23.play();
                        motionControl24.play();
                        
                        
                        
                        
                        
                        
                        
                    }
                }

                if (name.equals("SwitchPathInterpolation") && keyPressed) {
                    if (path.getPathSplineType() == Spline.SplineType.CatmullRom){
                        path.setPathSplineType(Spline.SplineType.Linear);
                    } else {
                        path.setPathSplineType(Spline.SplineType.CatmullRom);
                    }
                }

                if (name.equals("tensionUp") && keyPressed) {
                    path.setCurveTension(path.getCurveTension() + 0.1f);
                    System.err.println("Tension : " + path.getCurveTension());
                }
                if (name.equals("tensionDown") && keyPressed) {
                    path.setCurveTension(path.getCurveTension() - 0.1f);
                    System.err.println("Tension : " + path.getCurveTension());
                }


            }
        };

        inputManager.addListener(acl, "display_hidePath", "play_stop", "SwitchPathInterpolation", "tensionUp", "tensionDown");

    }
}