/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package networkMonitoring.AppStates;

/**
 *
 * @author xiaolong
 */
public enum InputMappingName {

	USE("use"),
	
	MOVE_LEFT("move_left"),
	MOVE_RIGHT("move_right"),
	MOVE_FORWARD("move_up"),
	MOVE_BACKWARD("move_down"),
	
	TURN_LEFT("turn_left"),
	TURN_RIGHT("turn_right"),
	MOUSE_LOOK_UP("mouse_look_up"),
	MOUSE_LOOK_DOWN("mouse_look_down"),
	
	LEFT_ALT("left_alt"),
	
	ENTER("enter"),
	
	JUMP("jump"),
	TOGGLE_DEBUG("toggle_debug"),
	PAN("pan"),
        RETURN_TO_WORLD("returnToWorld"),
        ANIMATION("packageAnimation");
	
	private final String text;

	private InputMappingName(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}