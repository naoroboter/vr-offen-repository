/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package networkMonitoring.AppStates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import graph.MonitoredHost;

/**
 *
 * @author hexmare
 */
public class WorldState extends BaseAppState {

	public BulletAppState bulletAppState;
	public Node rootNode, mainOffice;
	Spatial playerModel, officePCs, adminPC, server1, server2, basementWalls, receptionPCs;
	
	private Node usables;
	
	
	@Override
	protected void initialize(Application app) {
		
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		bulletAppState = new BulletAppState();
		bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
		getStateManager().attach(bulletAppState);
		rootNode = ((SimpleApplication) getApplication()).getRootNode();
		
		
		 usables = new Node("Shootables");

		//Spatial sceneModel = getApplication().getAssetManager().loadModel("Models/mainHouse/hausnew.j3o");
		mainOffice = (Node) getApplication().getAssetManager().loadModel("Models/mainHouse/hausnew.j3o");
		
		addPCs();
		addTransparentInteractionWalls();
		addMarkers();
		
		mainOffice.attachChild(adminPC);
		server1 = getApplication().getAssetManager().loadModel("Models/server1/server1.j3o");
		mainOffice.attachChild(server1);
		server2 = getApplication().getAssetManager().loadModel("Models/server2/server2.j3o");
		mainOffice.attachChild(server2);
		basementWalls = getApplication().getAssetManager().loadModel("Models/basementWalls/kellerwand.j3o");
		mainOffice.attachChild(basementWalls);
		receptionPCs = getApplication().getAssetManager().loadModel("Models/receptionPCs/newRec.j3o");
		mainOffice.attachChild(receptionPCs);

		CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) mainOffice);
		RigidBodyControl scene = new RigidBodyControl(sceneShape, 0);
		mainOffice.addControl(scene);
		bulletAppState.getPhysicsSpace().add(scene);
		Node sceneNode = new Node("scene node");
		rootNode.attachChild(sceneNode);
		sceneNode.attachChild(mainOffice);

//        Spatial terrain = ((Node)sceneModel).getChild("terrain-scene1");
//        terrain.addControl(new RigidBodyControl(0));
//        terrain.setShadowMode(RenderQueue.ShadowMode.Receive);
//        bulletAppState.getPhysicsSpace().addAll(terrain);


		//PointLight pl = new PointLight();
		//rootNode.addLight(pl);
		//pl.setPosition(new Vector3f(-6f,2f,-2f));


	}
	
	private void addPCs() {
		officePCs = getApplication().getAssetManager().loadModel("Models/officePCs/allPcsandTables.j3o");
		officePCs.setName("office_pcs");
		
		mainOffice.attachChild(officePCs);
		
		adminPC = getApplication().getAssetManager().loadModel("Models/adminPC/adminpc.j3o");
		adminPC.setName("admin_pc");
	}

	private void addTransparentInteractionWalls() {
		Spatial admin_pc_interactive_wall = loadModel("Models/AllWalls/NewAdmin/adminwall.j3o");
		
		Spatial pc_1_1 = loadModel("Models/AllWalls/1stock/pc1/1st1pc.j3o");
		Spatial pc_1_2 = loadModel("Models/AllWalls/1stock/pc2/1st2pc.j3o");
		Spatial pc_1_3 = loadModel("Models/AllWalls/1stock/pc3/1st3pc.j3o");
		Spatial pc_1_4 = loadModel("Models/AllWalls/1stock/pc4/1st4pc.j3o");
		Spatial pc_1_5 = loadModel("Models/AllWalls/1stock/pc5/1st5pc.j3o");
		Spatial pc_1_6 = loadModel("Models/AllWalls/1stock/pc6/1st6pc.j3o");
		Spatial pc_1_7 = loadModel("Models/AllWalls/1stock/pc7/1st7pc.j3o");
		Spatial pc_1_8 = loadModel("Models/AllWalls/1stock/pc8/1st8pc.j3o");
		
		Spatial pc_2_1 = loadModel("Models/AllWalls/2stock/pc1/2st1pc.j3o");
		Spatial pc_2_2 = loadModel("Models/AllWalls/2stock/pc2/2st2pc.j3o");
		Spatial pc_2_3 = loadModel("Models/AllWalls/2stock/pc3/2st3pc.j3o");
		Spatial pc_2_4 = loadModel("Models/AllWalls/2stock/pc4/2st4pc.j3o");
		Spatial pc_2_5 = loadModel("Models/AllWalls/2stock/pc5/2st5pc.j3o");
		Spatial pc_2_6 = loadModel("Models/AllWalls/2stock/pc6/2st6pc.j3o");
		Spatial pc_2_7 = loadModel("Models/AllWalls/2stock/pc7/2st7pc.j3o");
		Spatial pc_2_8 = loadModel("Models/AllWalls/2stock/pc8/2st8pc.j3o");
		
		Spatial pc_3_1 = loadModel("Models/AllWalls/3stock/pc1/3st1pc.j3o");
		Spatial pc_3_2 = loadModel("Models/AllWalls/3stock/pc2/3st2pc.j3o");
		Spatial pc_3_3 = loadModel("Models/AllWalls/3stock/pc3/3st3pc.j3o");
		Spatial pc_3_4 = loadModel("Models/AllWalls/3stock/pc4/3st4pc.j3o");
		Spatial pc_3_5 = loadModel("Models/AllWalls/3stock/pc5/3st5pc.j3o");
		Spatial pc_3_6 = loadModel("Models/AllWalls/3stock/pc6/3st6pc.j3o");
		Spatial pc_3_7 = loadModel("Models/AllWalls/3stock/pc7/3st7pc.j3o");
		Spatial pc_3_8 = loadModel("Models/AllWalls/3stock/pc8/3st8pc.j3o");
		
		
		pc_1_1.setName(MonitoredHost.TM0401.toString());
		pc_1_2.setName(MonitoredHost.TM0402.toString());
		pc_1_3.setName(MonitoredHost.TM0403.toString());
		pc_1_4.setName(MonitoredHost.TM0404.toString());
		pc_1_5.setName(MonitoredHost.TM0405.toString());
		pc_1_6.setName(MonitoredHost.TM0406.toString());
		pc_1_7.setName(MonitoredHost.TM0407.toString());
		
		pc_1_8.setName(MonitoredHost.TM0408.toString());
		admin_pc_interactive_wall.setName(MonitoredHost.LOCALHOST.toString());
		
		pc_2_1.setName(MonitoredHost.TM0409.toString());
		pc_2_2.setName(MonitoredHost.TM0410.toString());
		pc_2_3.setName(MonitoredHost.TM0411.toString());
		pc_2_4.setName(MonitoredHost.TM0412.toString());
		pc_2_5.setName(MonitoredHost.TM0413.toString());
		pc_2_6.setName(MonitoredHost.TM0414.toString());
		pc_2_7.setName(MonitoredHost.TM0415.toString());
		pc_2_8.setName(MonitoredHost.TM0416.toString());
		
		usables.attachChild(pc_1_1);
		usables.attachChild(pc_1_2);
		usables.attachChild(pc_1_3);
		usables.attachChild(pc_1_4);
		usables.attachChild(pc_1_5);
		usables.attachChild(pc_1_6);
		usables.attachChild(pc_1_7);
		usables.attachChild(pc_1_8);
		usables.attachChild(admin_pc_interactive_wall);
		
		usables.attachChild(pc_2_1);
		usables.attachChild(pc_2_2);
		usables.attachChild(pc_2_3);
		usables.attachChild(pc_2_4);
		usables.attachChild(pc_2_5);
		usables.attachChild(pc_2_6);
		usables.attachChild(pc_2_7);
		usables.attachChild(pc_2_8);
		
		usables.attachChild(pc_3_1);
		usables.attachChild(pc_3_2);
		usables.attachChild(pc_3_3);
		usables.attachChild(pc_3_4);
		usables.attachChild(pc_3_5);
		usables.attachChild(pc_3_6);
		usables.attachChild(pc_3_7);
		usables.attachChild(pc_3_8);
		
		mainOffice.attachChild(usables);
	}
	
	public Node getUsables() {
		return this.usables;
	}
	
	private void addMarkers() {
		for (int i = 1; i <= 3; i++) {
			for (int k = 1; k <= 8; k++) {
				Spatial marker = loadModel("Models/Pointers/"+i+"st/"+k+"pc/"+i+"st"+k+"pt.j3o");
				marker.setName(i + "" + k + "_marker");
				mainOffice.attachChild(marker);
			}
		}
	}
	
	public Node getRootNode() {
		return this.rootNode;
	}
	
    
     public Spatial getreceptionPCs() {
            return this.receptionPCs;
    }

    
//    public void detachSpatial(Application app){
//        app.getRootNode().detachChild(getAdminPc());
//
//    }

	private Spatial loadModel(String path) {
		return getApplication().getAssetManager().loadModel(path);
	}
	
	
	@Override
	protected void cleanup(Application app) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	protected void enable() {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	protected void disable() {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void update(float tpf) {
	}

	public Spatial getAdminPc() {
		return this.adminPC;
	}

}
