/*
 * $Id: PlayerState.java 1169 2013-10-05 06:43:37Z PSpeed42@gmail.com $
 *
 * Copyright (c) 2013 jMonkeyEngine
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'jMonkeyEngine' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package networkMonitoring.AppStates;

import graph.MonitoredHost;
import networkMonitoring.AMain.GameClientIntr;
import networkMonitoring.AMain.ClientMain;
import networkMonitoring.Networking.Msg.CommandSet;
import com.jme3.app.Application;
import com.jme3.audio.Listener;
import com.jme3.bullet.BulletAppState;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import graph.MonitoringGraphDrawer;
import java.util.logging.Level;
import java.util.logging.Logger;
import networkMonitoring.Animation.PackageAnimation;
import networkMonitoring.Factories.GameModelFactory;
import networkMonitoring.Objects.Avatar;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Paul Speed
 */
public class PlayerState extends BaseAppState implements ActionListener, AnalogListener { //implements AnalogFunctionListener {

	private GameClientIntr client;
	private EntityData entityData;
	private EntityId player;
	private Client networkClient;
	private Node interpNode;
	//private Position lastPos;
	private Quaternion cameraAngle;
	private Vector3f cameraDelta;
	private Vector3f audioDelta;
	private float cameraDistance = 15;
	private InputManager inputManager;
	// Here for the moment
	//private SensorArea sensor;
	private int xLast = -1;
	private int yLast = -1;
	private boolean fwd = false;
	private boolean rev = false;
	private boolean left = false;
	private boolean right = false;
	private boolean jump = false;
	private boolean updateCommand = false;
	private boolean pan = false;
	private boolean altPressed = false;
	private Listener audioListener = new Listener();
	private ClientMain app;
	
	private HashMap<Trigger, InputMappingName> inputMappingNameMap = new HashMap<>();
	private HashMap<MonitoredHost, String> hostNameIPMapping = new HashMap<>(); //TODO?
	
	MonitoringGraphDrawer monitoringGraphDrawer;
	
	public PlayerState(GameClientIntr client, Listener audioListener) {
		this.client = client;
		this.audioListener = audioListener;
	}

	public PlayerState(GameClientIntr client) {
		this.client = client;
		this.audioListener = null;
	}

	public GameClientIntr getClient() {
		return client;
	}

	public Listener getAudioListener() {
		return audioListener;
	}

	@Override
	protected void initialize(Application app) {
		this.app = (ClientMain) app;
		this.networkClient = getApplication().getStateManager().getState(ConnectionState.class).getClient();
		this.inputManager = app.getInputManager();
		inputManager.setCursorVisible(true);
		this.entityData = client.getEntityData();
		this.player = client.getPlayer();
		this. monitoringGraphDrawer = new MonitoringGraphDrawer(app);
		initializeActionListeners();
		addActionListeners();
	}

	private void initializeActionListeners() {
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_A), InputMappingName.MOVE_LEFT);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_D), InputMappingName.MOVE_RIGHT);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_S), InputMappingName.MOVE_BACKWARD);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_W), InputMappingName.MOVE_FORWARD);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_SPACE), InputMappingName.JUMP);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_F12), InputMappingName.TOGGLE_DEBUG);
		
		inputMappingNameMap.put(new MouseButtonTrigger(MouseInput.BUTTON_RIGHT), InputMappingName.PAN);
		
		inputMappingNameMap.put(new MouseAxisTrigger(MouseInput.AXIS_X, true), InputMappingName.TURN_LEFT);
		inputMappingNameMap.put(new MouseAxisTrigger(MouseInput.AXIS_X, false), InputMappingName.TURN_RIGHT);
		inputMappingNameMap.put(new MouseAxisTrigger(MouseInput.AXIS_Y, true), InputMappingName.MOUSE_LOOK_DOWN);
		inputMappingNameMap.put(new MouseAxisTrigger(MouseInput.AXIS_Y, false), InputMappingName.MOUSE_LOOK_UP);
		
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_LMENU), InputMappingName.LEFT_ALT);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_RETURN), InputMappingName.ENTER);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_E), InputMappingName.USE);
                inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_R), InputMappingName.RETURN_TO_WORLD);
		inputMappingNameMap.put(new KeyTrigger(KeyInput.KEY_C), InputMappingName.ANIMATION);
	}

	private void addActionListeners() {
		for (Map.Entry<Trigger, InputMappingName> entry : inputMappingNameMap.entrySet()) {
			inputManager.addMapping(entry.getValue().toString(), entry.getKey());
			inputManager.addListener(this, entry.getValue().toString());
		}
	}

	@Override
	public void onAction(String binding, boolean isPressed, float tpf) {
		
		if (binding.equals(InputMappingName.MOVE_LEFT.toString())) {
			left = isPressed;
			updateCommand = true;

		} else if (binding.equals(InputMappingName.MOVE_RIGHT.toString())) {
			right = isPressed;
			updateCommand = true;
		} else if (binding.equals(InputMappingName.MOVE_FORWARD.toString())) {
			fwd = isPressed;
			updateCommand = true;
		} else if (binding.equals(InputMappingName.MOVE_BACKWARD.toString())) {
			rev = isPressed;
			updateCommand = true;
		} else if (binding.equals(InputMappingName.TOGGLE_DEBUG.toString())) {
			if (!isPressed) {
				BulletAppState bs = getApplication().getStateManager().getState(BulletAppState.class);
				bs.setDebugEnabled(!bs.isDebugEnabled());
			}
		} else if (binding.equals(InputMappingName.JUMP.toString())) {
			jump = isPressed;
			updateCommand = true;

		} else if (binding.equals(InputMappingName.LEFT_ALT.toString())) {
			altPressed = isPressed;


		} else if (binding.equals(InputMappingName.ENTER.toString()) && altPressed) {
			if (isPressed) {
				boolean isFullScreen = ((ClientMain) getApplication()).getIsFullScreen();
				AppSettings newAppSettings = new AppSettings(true);
				if (isFullScreen) {
					newAppSettings.setResolution(1024, 768);
				} else {
					newAppSettings.setResolution(1920, 1080);
				}

				newAppSettings.setFullscreen(!isFullScreen);
				((ClientMain) getApplication()).setIsFullScreen(!isFullScreen);
				getApplication().setSettings(newAppSettings);
				getApplication().restart();
			}


		} else if (binding.equals(InputMappingName.PAN.toString())) {
			pan = isPressed;
			inputManager.setCursorVisible(!isPressed);
			updateCommand = true;

		} else if (binding.equals(InputMappingName.USE.toString()) && isPressed) {
			useActionPerformed();
			updateCommand = true;
		}else if (binding.equals(InputMappingName.ANIMATION.toString()) && isPressed) {
                    Spatial officePCs,adminPC,server1,server2;
                    officePCs = getApplication().getAssetManager().loadModel("Models/officePCs/allPcsandTables.j3o");
                    adminPC   = getApplication().getAssetManager().loadModel("Models/adminPC/adminpc.j3o");
                    server1   = getApplication().getAssetManager().loadModel("Models/server1/server1.j3o");
                    server2   = getApplication().getAssetManager().loadModel("Models/server2/server2.j3o"); 

                    System.out.println("detachAllChildren()");
                    app.getRootNode().detachAllChildren();
//                    WorldState worldState = app.getStateManager().getState(WorldState.class);
//                    app.getRootNode().detachChild(worldState.getreceptionPCs());
                   
                    System.out.println("attach_required_Children()");
                    app.getRootNode().attachChild(officePCs);
                    app.getRootNode().attachChild(adminPC);
                    app.getRootNode().attachChild(server1);
                    app.getRootNode().attachChild(server2);
                    
                    // fly Camera
                    app.getFlyByCamera().setMoveSpeed(30);
                    PackageAnimation anim =new PackageAnimation(app.getRootNode(), app.getAssetManager(),app.getInputManager());
                    //anim.startAnimation(app.getRootNode(), app.getAssetManager(),app.getInputManager());
		}else if (binding.equals(InputMappingName.RETURN_TO_WORLD.toString()) && isPressed) {
                    System.out.println("Button RRR was pressed!!");
                     app.getRootNode().detachAllChildren();
                     Spatial mainOffice;
                     mainOffice = getApplication().getAssetManager().loadModel("Models/mainHouse/hausnew.j3o");
                     app.getRootNode().attachChild(mainOffice);
//                     Avatar avat=new Avatar("Avatar",app);
//                     app.getStateManager().attach(new ModelState(time, new GameModelFactory(this, null, time),systems.getEntityData(),true));
		}
	}

	@Override
	public void onAnalog(String binding, float value, float tpf) {
		if (pan) {
			getApplication().getStateManager().getState(ModelState.class).setAvatarDirection(binding, value, tpf);
			updateCommand = true;
		}
	}

	@Override
	protected void cleanup(Application app) {
	}

	@Override
	public void update(float tpf) {
		Camera cam = getApplication().getCamera();
		if (updateCommand) {
			updateCommand = false;
			CommandSet cs = new CommandSet(player, fwd, rev, left, right, jump);
			networkClient.send(cs);
			getApplication().getStateManager().getState(ModelState.class).setAvatarCommand(cs);
		}

	}

	@Override
	protected void enable() {
	}

	@Override
	protected void disable() {
	}

	private void useActionPerformed() {
		MonitoredHost focusedHost = getFocusedMonitoredHost();
		monitoringGraphDrawer.toggleMonitoringGraph(focusedHost);
	}
	
	private MonitoredHost getFocusedMonitoredHost() {
		WorldState worldState = app.getStateManager().getState(WorldState.class);
		Camera cam = app.getCamera();
		MonitoredHost focusedHost = null;
		
		// 1. Reset results list.
		CollisionResults results = new CollisionResults();
		CollisionResult closest = null;
		
		// 2. Aim the ray from cam loc to cam direction.
		Ray ray = new Ray(cam.getLocation(), cam.getDirection());
		// 3. Collect intersections between Ray and Shootables in results list.
		Node useables = worldState.getUsables();
		useables.collideWith(ray, results);
		
		// 4. Print the results
		/*System.out.println("----- Collisions? " + results.size() + "-----");
		for (int i = 0; i < results.size(); i++) {
			// For each hit, we know distance, impact point, name of geometry.
			float dist = results.getCollision(i).getDistance();
			Vector3f pt = results.getCollision(i).getContactPoint();
			String hit = results.getCollision(i).getGeometry().getName();
			System.out.println("* Collision #" + i);
			System.out.println("  You shot " + hit + " at " + pt + ", " + dist + " wu away.");
		}*/
		// 5. Use the results (we mark the hit object)
		if (results.size() > 0) {
			// The closest collision point is what was truly hit:
			closest = results.getClosestCollision();
			// Let's interact - we mark the hit with a red dot.
			app.getMark().setLocalTranslation(closest.getContactPoint());
			worldState.getRootNode().attachChild(app.getMark());
		} else {
			// No hits? Then remove the red mark.
			worldState.getRootNode().detachChild(app.getMark());
		}
		
		
		if(closest != null) {
			// find the monitored host, with the name of the hit transparent wall
			for (MonitoredHost host : MonitoredHost.values()) {
				String hostName = host.toString();
				
				// go up through all parents, to see if one of them has the name of the host
				Node currentNode = closest.getGeometry().getParent();
				do {
					String hitTarget = currentNode.getName();

					if(hitTarget.equals(hostName)) {
						focusedHost = host;
					} else {
						System.out.println("The hit target has the name:|"+hitTarget+"|");
						System.out.println("The host has the name:|"+hostName+"|");
					}
					
					// update parent, loop condition
					currentNode = currentNode.getParent();
					
				} while(currentNode.getParent() != null);
			}
		} else {
			System.out.println("There is no closest geometry!");
		}
		
		if(focusedHost != null) {
			System.out.println("Hit MonitoredHost: " + focusedHost.toString());
		} else {
			System.out.println("You didn't hit a MonitoredHost!");
		}
		
		return focusedHost;
	}
}
