package networkMonitoring.AMain;

import networkMonitoring.AppStates.ConnectionState;
import networkMonitoring.AppStates.MainMenuState;
import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;

public class ClientMain extends SimpleApplication {

	//private AppSettings settings;
	private boolean isFullScreen = false;
	//private static final boolean LOAD_DEFAULT_SETTINGS = true;

	public static void main(String[] args) {
		ClientMain app = new ClientMain();
		//AppSettings settings = new AppSettings(LOAD_DEFAULT_SETTINGS);
		//settings.setResolution(1280,1024);
		app.setPauseOnLostFocus(true);
		//app.setDisplayStatView(true);
		//app.setDisplayFps(true);
		app.start();
	}
	private BitmapText crosshair;
	private Geometry mark;

	public ClientMain() {
		super();
	}

	public boolean getIsFullScreen() {
		return isFullScreen;
	}

	public void setIsFullScreen(boolean value) {
		this.isFullScreen = value;
	}

	@Override
	public void simpleInitApp() {
		getStateManager().attach(new MainMenuState());
		initCrossHairs();
		initMark();
		initSky();

	}

	@Override
	public void simpleUpdate(float tpf) {
		//TODO: add update code
	}

	@Override
	public void simpleRender(RenderManager rm) {
		//TODO: add render code
	}

	@Override
	public void destroy() {
		try {
			getStateManager().getState(ConnectionState.class).cleanup();
		} catch (Exception e) {
		}
		super.destroy();
	}

	/**
	 * A red ball that marks the last spot that was "hit" by the "shot".
	 */
	protected void initMark() {
		Sphere sphere = new Sphere(30, 30, 0.2f);
		mark = new Geometry("BOOM!", sphere);
		Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mark_mat.setColor("Color", ColorRGBA.Red);
		mark.setMaterial(mark_mat);
	}

	/**
	 * A centred plus sign to help the player aim.
	 */
	protected void initCrossHairs() {
		if (this.settings == null) {
			System.out.println("settings is null");
		}

		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		crosshair = new BitmapText(guiFont, false);
		crosshair.setSize(guiFont.getCharSet().getRenderedSize() * 2);
		crosshair.setText("+"); // crosshairs
		crosshair.setLocalTranslation( // center
				this.settings.getWidth() / 2 - crosshair.getLineWidth() / 2, this.settings.getHeight() / 2 + crosshair.getLineHeight() / 2, 0);
		guiNode.attachChild(crosshair);
	}
	
	private void initSky() {
		Spatial sky = assetManager.loadModel("/Scenes/Sky.j3o");
		rootNode.attachChild(sky);
	}
	
	/**
	 * @return the crosshair
	 */
	public BitmapText getCrosshair() {
		return crosshair;
	}

	/**
	 * @param crosshair the crosshair to set
	 */
	public void setCrosshair(BitmapText crosshair) {
		this.crosshair = crosshair;
	}

	/**
	 * @return the mark
	 */
	public Geometry getMark() {
		return mark;
	}

	/**
	 * @param mark the mark to set
	 */
	public void setMark(Geometry mark) {
		this.mark = mark;
	}
	
	public AppSettings getSettings() {
		return this.settings;
	}
}
