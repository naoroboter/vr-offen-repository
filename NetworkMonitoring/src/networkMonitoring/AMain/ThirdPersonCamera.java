package networkMonitoring.AMain;

import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.control.CameraControl;

public class ThirdPersonCamera {
	
	private static final float CAMERA_FOLLOW_DISTANCE = -1f;
	private static final float CAMERA_HEIGHT = 1.2f;
	private static final float PIVOT_HEIGHT = 1.2f;
	private static final Vector3f CAMERA_TRANSLATION = new Vector3f(0, CAMERA_HEIGHT, CAMERA_FOLLOW_DISTANCE);
	private static final Vector3f PIVOT_TRANSLATION = new Vector3f(0, PIVOT_HEIGHT, 1);
	private static final String PIVOT_NODE_NAME = "CamTrack";
	
	private Node pivot;
	
	private CameraNode cameraNode;
	
	//Change these as you desire. Lower verticalAngle values will put the camera
	//closer to the ground.

	public float verticalAngle = 45 * FastMath.DEG_TO_RAD;
	
	//These bounds keep the camera from spinning too far and clipping through
	//the floor or turning upside-down. You can change them as needed but it is
	//recommended to keep the values in the (-90,90) range.
	public float maxVerticalAngle = 85 * FastMath.DEG_TO_RAD;
	public float minVerticalAngle = -20 * FastMath.DEG_TO_RAD;

	public ThirdPersonCamera(String name, Camera cam, Node player) {
		pivot = new Node(PIVOT_NODE_NAME);
		pivot.setLocalTranslation(PIVOT_TRANSLATION);
		player.attachChild(pivot);

		//create the camera Node
		cameraNode = new CameraNode(name, cam);
		
		//This mode means that camera copies the movements of the target:
		cameraNode.setControlDir(CameraControl.ControlDirection.SpatialToCamera);
		
		//Attach the camNode to the target:
		pivot.attachChild(cameraNode);
		cameraNode.setLocalTranslation(CAMERA_TRANSLATION);
		cameraNode.lookAt(pivot.getLocalTranslation(), Vector3f.UNIT_Y);
	}

	public void verticalRotate(float angle) {
		verticalAngle += angle;

		if (verticalAngle > maxVerticalAngle) {
			verticalAngle = maxVerticalAngle;
		} else if (verticalAngle < minVerticalAngle) {
			verticalAngle = minVerticalAngle;
		}

		pivot.getLocalRotation().fromAngleAxis(-verticalAngle, Vector3f.UNIT_X);
	}

	public CameraNode getCameraNode() {
		return cameraNode;
	}

	public Node getCameraTrack() {
		return pivot;
	}
}