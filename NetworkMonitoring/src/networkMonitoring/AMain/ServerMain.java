package networkMonitoring.AMain;

import networkMonitoring.AppStates.ModelState; // Entity (the object of the world) and it describes the options
import networkMonitoring.AppStates.WorldState; // Main stage i.e. our office
import networkMonitoring.Factories.GameModelFactory; // call and creating an avatar
import networkMonitoring.Handlers.GameMessageHandler; // processing of messages between clients
import networkMonitoring.Networking.Util; // Options to connect to the server and ports
import networkMonitoring.Services.GameSystems; // game service management: obtaining removal services
import networkMonitoring.Services.Service; // interface for initialization or upgrade services
import com.jme3.app.SimpleApplication;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.renderer.RenderManager;
import com.jme3.system.JmeContext;
import com.simsilica.es.net.EntitySerializers;
import com.simsilica.es.server.EntityDataHostService;
import com.simsilica.es.server.SessionDataDelegator;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ServerMain extends SimpleApplication {
    
    private GameSystems systems; //contains a part of the logic of the game: Control services (eg: start (); stop ()); Time (eg: getGameTime ())
    private int port;       // port number to connect
    private Server host;    // Creating an object-server contains some of methods (eg: getGameName (); broadcast (Message message) and so on) 
    private EntityDataHostService edHost; // Service of management of the world (eg: start/stop connections with the object). Also, the server host is passed
    // Entity - world object like character, vehicle, box or factory
    private ConnectionObserver connectionObserver = new ConnectionObserver();

    static {
        EntitySerializers.initialize();
        Util.initializeSerializables();
    }
    
    public static void main(String[] args) {
        ServerMain app = new ServerMain(); // creation of the application itself
        app.setPauseOnLostFocus(false);     // default=enabled -> the application will work even alt+tab was pressed
        app.start(JmeContext.Type.Headless); // launch the application. Automatically starts simpleInitApp ()
    }

    @Override
    public void simpleInitApp() {
        // start services management of the world (Entity), time management (periodic updates of what is happening in the game)
        this.systems = new GameSystems(this);
        
        try {
            // Create the network hosting connection
            host = Network.createServer(Util.GAME_NAME,    
                                        Util.PROTOCOL_VERSION, 
                                        Util.tcpPort, Util.udpPort);
        } catch (IOException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        // the server itself needs also the channel, so add it to channel "1338"                        
        System.out.println("Adding channel 0 on port:" + (Util.tcpPort+1));                                    
        host.addChannel(Util.tcpPort+1);
 
        // Add our own stub service to send updates at the end of
        // the game update cycle
        systems.addService(new Service() {
                public void update( long gameTime ) {
                    edHost.sendUpdates();
                }

                public void initialize( GameSystems systems ) {
                    // Setup the network listeners
                    edHost = new EntityDataHostService(host, 0, systems.getEntityData());                 
                }

                public void terminate( GameSystems systems ) {
                    // Remove the listeners for the es hosting
                    edHost.stop();        
                }
            });
        
        // Start the game systems
        systems.start();

        TimeProvider time = systems.getGameTimeProvider(); // TimeProvider interface: contains a method return time
        // Adding the main stage. i.e. in this case, the office 
        getStateManager().attach(new WorldState());
        // Adding the hero, with the transfer of the time-object 
        getStateManager().attach(new ModelState(time, new GameModelFactory(this, null, time),systems.getEntityData(),true));
        //getStateManager().attach(new MovementAppState(systems.getEntityData(),systems));
        // Will delegate certain messages to the GameMessageHandler for
        // a particular connection.
        SessionDataDelegator delegator = new SessionDataDelegator(GameMessageHandler.class, GameMessageHandler.ATTRIBUTE, true);
        host.addMessageListener(delegator, delegator.getMessageTypes());
 
        // Add our own connection listener that will add GameMessageHandlers
        // to connections.
        host.addConnectionListener(connectionObserver);
        
        // Start accepting connections
        host.start();
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    @Override
    // shutdown the server
    public void destroy()
    {
        // Stop accepting network connections and kick any
        // existing clients.
        host.close();
 
        // Shut down the game systems
        systems.stop();
        super.destroy();
    }
    
    // Класс следит за подключением/откл. клиентов. Правилное открытие/закрытие соединения
    private class ConnectionObserver implements ConnectionListener {

        public void connectionAdded(Server server, HostedConnection hc) {
            addConnection(hc);
        }

        public void connectionRemoved(Server server, HostedConnection hc) {
            removeConnection(hc);
        }
    }
    
    // The class follows the connecting and disconnecting customers. The proper opening / closing connection
    protected void addConnection( HostedConnection conn ) {
        System.out.println( "New connection from:" + conn );
        GameMessageHandler handler = new GameMessageHandler(systems, conn);
        conn.setAttribute(GameMessageHandler.ATTRIBUTE, handler);
    }
    // Method for remote clients
    protected void removeConnection( HostedConnection conn ) {
        System.out.println( "Connection closed:" + conn );        
        GameMessageHandler handler = conn.getAttribute(GameMessageHandler.ATTRIBUTE);
        if( handler != null ) {
            handler.close();
        }
    }
    
//    public GameSystems getSystems(){
//        return this.systems; 
//    }
    
}
