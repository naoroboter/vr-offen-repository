package examples;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.CPUUsageData;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.RandomHelper;
import java.io.File;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

import javax.imageio.ImageIO;

public class SceneToImageAndWrite extends Application {
	
	AreaChart<String, Number> areaChart;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("CPU Usage");

		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel("t(s)");
		
		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("%");

		areaChart = new AreaChart<>(xAxis, yAxis);
		areaChart.setTitle("CPU Usage");

		Series<String, Number> series = createSampleData();
		
		
		// LINE CHART STUFF
		areaChart.getData().add(series);
		
		areaChart.setLegendVisible(true);
		areaChart.setLegendSide(Side.BOTTOM);
		
		areaChart.setAnimated(false);
		areaChart.setCreateSymbols(false);
		
		
		// DISPLAY
		Scene scene = new Scene(areaChart, 1280, 480);
		
		try {
			scene.getStylesheets().add(getClass().getResource("line-chart-style.css").toExternalForm());
		} catch(Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}

		stage.setScene(scene);
		stage.show();

		WritableImage snapShot = scene.snapshot(null);
		
		ImageIO.write(SwingFXUtils.fromFXImage(snapShot, null), "png", new File("cpu-usage.png"));
	}
	
	/**
	 * This method updates the chart with a list of CPU usage data.
	 * Each data will be one area in the AreaChart.
	 * @param data a list of CPU usage data
	 */
	public void updateChart(final ObservableList<CPUUsageData> data) {
		areaChart.getData().clear();
		data.stream().forEach((item) -> {
			areaChart.getData().addAll(item.getSeries());
		});
	}
	
	public Series<String, Number> createSampleData() {
		XYChart.Series series = new Series();
		series.setName("CPU usage over time");

		LinkedHashMap<String, Integer> map = new LinkedHashMap();
		for (int i = 0; i < 100; i++) {
			LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(i);
			String timeString = localDateTime.getHour() + ":" + localDateTime.getMinute() + ":" + localDateTime.getSecond();
			map.put(timeString, RandomHelper.getRandomNumber(0, 100));
		}
		
		map.entrySet().stream().forEach((Entry<String, Integer> entry) -> {
			Data chartData = new XYChart.Data(entry.getKey(), entry.getValue());
			series.getData().add(chartData);
		});
		
		return series;
	}
}
