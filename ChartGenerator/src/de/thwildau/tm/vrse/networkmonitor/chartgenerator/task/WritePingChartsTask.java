/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.ImageFileWriter;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.TimeHelper;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.util.MaxSizeHashMap;
import de.thwildau.tm.vrse.networkmonitor.model.Result;
import java.util.HashMap;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;

/**
 *
 * @author xiaolong
 */
public class WritePingChartsTask extends TimerTask {
	
	private static final String CSS_FILE_NAME = "ping-chart-style.css";
	private static final String FILE_PREFIX = "ping-";
	private static final String DIAGRAM_TITLE_PREFIX = "Ping of ";
	private static final String DIAGRAM_X_AXIS_LABEL = "Time in HH:mm:ss";
	private static final String DIAGRAM_Y_AXIS_LABEL = "Latency in s";
	
	private static final int MAX_MAP_CAPACITY = 400;
	public static final long WRITE_PING_CHARTS_INTERVAL = 5000;
	
	// each host gets its own stage, scene, areachart and cpu-data-series
	private static final HashMap<String, Scene> ping_scenes = new HashMap<>();
	private static final HashMap<String, LineChart<String, Number>> ping_charts = new HashMap<>();
	private static final HashMap<String, Stage> ping_stages = new HashMap<>();
	
	private static final HashMap<String, MaxSizeHashMap<String, Double>> ping_data_latency = new HashMap<>();
	private static final HashMap<String, MaxSizeHashMap<String, Double>> ping_data_loss = new HashMap<>();
	
	private static final int SCENE_HEIGHT = 480;
	private static final int SCENE_WIDTH = 1280;
	
	@Override
	public void run() {
		WebserviceDataGetterTask.getResultList().stream().forEach(
				result -> readPingDataFromResult(result)
		);
		/*
		Using: Platform.runLater(new Runnable() {...});
		is necessary to run the TimerTask on the JavaFX Application Thread!
		*/
        Platform.runLater(new Runnable() {
			@Override
            public void run() {
				renderPingCharts();
			}
		});
	}
	
	/**
	 * This method reads CPU usage data from a given Result.
	 * Open question: How to make it so, that either the time between the values is always equal, 
	 * or the values are aligned on the x axis in a way, that results in a linear axis?
	 * -- Idea: Transform data into a time type which is known to JavaFX Diagrams
	 * -- Idea: Transform data into numbers somehow, maybe milliseconds and add a popup when hovering which shows a datetime string
	 * @param result the Result from which CPU data will be read.
	 */
	private void readPingDataFromResult(Result result) {
		String hostname = result.getIdentity().getHostName();
		
		System.out.println("HOSTNAME:" + hostname);
		
		if(!ping_data_latency.containsKey(hostname)) {
			MaxSizeHashMap<String, Double> ping_maxSizeHashMap = new MaxSizeHashMap<>(WritePingChartsTask.MAX_MAP_CAPACITY);
			ping_data_latency.put(hostname, ping_maxSizeHashMap);
		}
		
		if(!ping_data_loss.containsKey(hostname)) {
			MaxSizeHashMap<String, Double> ping_maxSizeHashMap = new MaxSizeHashMap<>(WritePingChartsTask.MAX_MAP_CAPACITY);
			ping_data_loss.put(hostname, ping_maxSizeHashMap);
		}
		
		if(result.getResource().getNetwork() != null) {
			if(result.getResource().getNetwork().getLatency() != -1) {
				// add value to the map of this host
				ping_data_latency.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						result.getResource().getNetwork().getLatency()
				);
			} else {
				ping_data_latency.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						new Double(0)
				);
			}
			
			if(result == null) {
				System.out.println("result is null");
			} else if (result.getResource() == null) {
				System.out.println("getResource is null");
			} else if (result.getResource().getNetwork() == null) {
				System.out.println("getNet is null");
			}
			
			if(result.getResource().getNetwork().getLossPercent() != -1) {
				// add value to the map of this host
				ping_data_loss.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						result.getResource().getNetwork().getLossPercent() / 100.0
				);
			} else {
				ping_data_loss.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						new Double(0)
				);
			}
		}
	}
	
	/**
	 * This method renders a CPU usage chart for all hosts and saves the rendered chart as a PNG file.
	 */
	private void renderPingCharts() {
		
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			String hostname = result.getIdentity().getHostName();
			
			XYChart.Series<String, Number> latency_series = new Series<>();
			XYChart.Series<String, Number> loss_series = new Series<>();
			
			// for each host create its own series of data for its chart
			ping_data_latency.get(hostname).entrySet().forEach((pingDataEntry) -> {
				XYChart.Data chartData = new XYChart.Data(pingDataEntry.getKey(), pingDataEntry.getValue());
				latency_series.getData().add(chartData);
				latency_series.setName("Latency");
			});
			
			// for each host create its own series of data for its chart
			ping_data_loss.get(hostname).entrySet().forEach((pingDataEntry) -> {
				XYChart.Data chartData = new XYChart.Data(pingDataEntry.getKey(), pingDataEntry.getValue());
				loss_series.getData().add(chartData);
				loss_series.setName("Loss");
			});
			
			// create the chart for each host
			if(!ping_charts.containsKey(hostname)) {
				LineChart<String, Number> pingLineChart = initializePingLineChart(
						WritePingChartsTask.DIAGRAM_TITLE_PREFIX + hostname, 
						WritePingChartsTask.DIAGRAM_Y_AXIS_LABEL, 
						WritePingChartsTask.DIAGRAM_X_AXIS_LABEL
				);
				
				pingLineChart.getData().add(latency_series);
				pingLineChart.getData().add(loss_series);

				pingLineChart.setLegendVisible(true);
				pingLineChart.setLegendSide(Side.BOTTOM);

				pingLineChart.setAnimated(false);
				pingLineChart.setCreateSymbols(false);
				ping_charts.put(hostname, pingLineChart);
			}
			
			// create the stage for each host
			if(!ping_stages.containsKey(hostname)) {
				Stage stage = new Stage();
				ping_stages.put(hostname, stage);
			}
			
			// create the scene for each host
			if(!ping_scenes.containsKey(hostname)) {
				Scene scene = new Scene(ping_charts.get(hostname), WritePingChartsTask.SCENE_WIDTH, WritePingChartsTask.SCENE_HEIGHT);
				scene.getStylesheets().add(getClass().getResource(WritePingChartsTask.CSS_FILE_NAME).toExternalForm());
				ping_scenes.put(hostname, scene);
				
				ping_stages.get(hostname).setScene(scene);
			}
			
			// always update the series of the charts!
			ping_charts.get(hostname).getData().clear();
			ping_charts.get(hostname).getData().add(latency_series);
			ping_charts.get(hostname).getData().add(loss_series);
			
			ImageFileWriter.writeToFile(ping_scenes.get(hostname), hostname, WritePingChartsTask.FILE_PREFIX);
		});
	}
	
	/**
	 * This method initializes a CPU usage chart.
	 * @param title the title of the chart
	 * @param yAxisLabel the label of the y-axis
	 * @param xAxisLabel the label of the x-axis
	 */
	private LineChart<String, Number> initializePingLineChart(String title, String yAxisLabel, String xAxisLabel) {
		LineChart<String, Number> pingLineChart;
		
		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel(xAxisLabel);

		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel(yAxisLabel);

		pingLineChart = new LineChart<>(xAxis, yAxis);
		pingLineChart.setTitle(title);
		
		return pingLineChart;
	}
}