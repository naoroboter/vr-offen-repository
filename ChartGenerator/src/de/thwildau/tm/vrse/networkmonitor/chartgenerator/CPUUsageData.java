/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

/**
 *
 * @author xiaolong
 */
public class CPUUsageData {
	private ObservableList<XYChart.Series<String, Number>> series;
	
	public CPUUsageData(ObservableList<XYChart.Series<String, Number>> series) {
		this.series = series;
	}
	
	public ObservableList<XYChart.Series<String, Number>> getSeries() {
		return series;
	}
}
