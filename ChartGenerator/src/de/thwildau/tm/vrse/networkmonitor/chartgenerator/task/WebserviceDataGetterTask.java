/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thwildau.tm.vrse.networkmonitor.model.Computer;
import de.thwildau.tm.vrse.networkmonitor.model.Result;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;

/**
 *
 * @author xiaolong
 */
public class WebserviceDataGetterTask extends TimerTask {
	
	private static List<Result> resultList = new ArrayList<>();
	
	public static final String URL_AS_STRING = "http://194.95.45.148:5000/get/all";
	public static final long GET_DATA_INTERVAL = 5000;
	
	/**
	 * This method returns a list of Results from a JSON, provided by a web service.
	 * @param url the URL of the web service
	 */
	public void getDataFromJSON(URL url) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Computer computers = mapper.readValue(url, Computer.class);
			resultList = computers.getResult();

		} catch (MalformedURLException ex) {
			Logger.getLogger(Computer.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Computer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void run() {
		System.out.println("getting data from webservice at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
		try {
			getDataFromJSON(new URL(WebserviceDataGetterTask.URL_AS_STRING));
		} catch (MalformedURLException ex) {
			Logger.getLogger(WebserviceDataGetterTask.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		/*
		 Platform.runLater(new Runnable() {
			@Override
            public void run() {
				System.out.println("getting data from webservice at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
				try {
					getDataFromJSON(new URL(WebserviceDataGetterTask.URL_AS_STRING));
				} catch (MalformedURLException ex) {
					Logger.getLogger(WebserviceDataGetterTask.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
        });
		*/
	}
	
	public static List<Result> getResultList() {
		return resultList;
	}
}
