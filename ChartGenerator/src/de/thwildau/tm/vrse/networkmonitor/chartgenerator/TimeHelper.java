/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator;

import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 *
 * @author xiaolong
 */
public class TimeHelper {
	public static String getCurrentTimeString() {
		LocalTime localDateTime = LocalDateTime.now().toLocalTime();
		String timeString = localDateTime.getHour() + ":" + localDateTime.getMinute() + ":" + localDateTime.getSecond();
		return timeString;
    }
}
