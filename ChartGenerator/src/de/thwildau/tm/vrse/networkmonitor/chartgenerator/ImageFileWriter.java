/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator;

import java.util.concurrent.CountDownLatch;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;

/**
 *
 * @author xiaolong
 */
public class ImageFileWriter {
	
	private static final String FILE_TYPE = "PNG";
	
	public static void writeToFile(Scene scene, String hostName, String prefix) {
		String fileName = prefix + hostName;
		WritableImage snapShot = scene.snapshot(null);
		
		CountDownLatch latch = new CountDownLatch(1);
		ImageFileWriterRunnable imageFileWriterRunnable = new ImageFileWriterRunnable(latch, fileName, ImageFileWriter.FILE_TYPE, snapShot);
		imageFileWriterRunnable.run();
	}
}
