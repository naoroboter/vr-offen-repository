/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.ImageFileWriter;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.TimeHelper;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.util.MaxSizeHashMap;
import de.thwildau.tm.vrse.networkmonitor.model.Result;
import java.util.HashMap;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;

/**
 *
 * @author xiaolong
 */
public class WriteUserChartsTask extends TimerTask {
	
	private static final int MAX_MAP_CAPACITY = 50;
	public static final long WRITE_USER_CHARTS_INTERVAL = 10000;
	
	// each host gets its own stage, scene, areachart and cpu-data-series
	private static final HashMap<String, Scene> user_count_scenes = new HashMap<>();
	private static final HashMap<String, BarChart<String, Number>> user_count_charts = new HashMap<>();
	private static final HashMap<String, Stage> user_count_stages = new HashMap<>();
	private static final HashMap<String, MaxSizeHashMap<String, Integer>> user_count_data = new HashMap<>();
	
	private static final int SCENE_HEIGHT = 480;
	private static final int SCENE_WIDTH = 1280;
	
	private static final String FILE_NAME_PREFIX = "user-count-";
	
	private static final String DIAGRAM_TITLE = "Logged In Users";
	private static final String DIAGRAM_X_AXIS_TITLE = "Time in HH:mm:ss";
	private static final String DIAGRAM_Y_AXIS_TITLE = "Logged In Users";
	
	private static final String CSS_FILE_NAME = "user-count-bar-chart-style.css";
	
	@Override
	public void run() {
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			readUserCountDataFromResult(result);
		});
		/*
		Using: Platform.runLater(new Runnable() {...});
		is necessary to run the TimerTask on the JavaFX Application Thread!
		*/
        Platform.runLater(new Runnable() {
			@Override
            public void run() {
				//System.out.println("TIMER TASK at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
				// render work
				renderUserCountCharts();
			}
		});
	}
	
	private void readUserCountDataFromResult(Result result) {
		//System.out.println("USER COUNT DATA read at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
		
		String hostname = result.getIdentity().getHostName();
		if(!user_count_data.containsKey(hostname)) {
			//System.out.println("Reading USER COUNT data of host: " + hostname);
			MaxSizeHashMap<String, Integer> user_count_maxSizeHashMap = new MaxSizeHashMap<>(WriteUserChartsTask.MAX_MAP_CAPACITY);
			user_count_data.put(hostname, user_count_maxSizeHashMap);
		}
		
		if(result.getResource().getUsers() != null) {
			if(result.getResource().getUsers().getCount().getLoggedIn() != -1) {
				// add value to the map of this host
				user_count_data.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						result.getResource().getUsers().getCount().getLoggedIn()
				);
			} else {
				//System.out.println("Writing 0 as value");
				user_count_data.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						0
				);
			}
		}
	}
	
	private void renderUserCountCharts() {
		
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			String hostname = result.getIdentity().getHostName();
			
			XYChart.Series<String, Number> series = new Series<>();
			
			// for each host create its own series of data for its chart
			user_count_data.get(hostname).entrySet().forEach((cpuUsageDataEntry) -> {
				XYChart.Data chartData = new XYChart.Data(cpuUsageDataEntry.getKey(), cpuUsageDataEntry.getValue());
				series.getData().add(chartData);
				series.setName("Users");
			});
			
			// create the chart for each host
			if(!user_count_charts.containsKey(hostname)) {
				BarChart<String, Number> userCountBarChart = initializeUserCountBarChart(
						WriteUserChartsTask.DIAGRAM_TITLE + " of " + hostname, 
						WriteUserChartsTask.DIAGRAM_Y_AXIS_TITLE, 
						WriteUserChartsTask.DIAGRAM_X_AXIS_TITLE
				);
				userCountBarChart.getData().add(series);

				userCountBarChart.setLegendVisible(true);
				userCountBarChart.setLegendSide(Side.BOTTOM);

				userCountBarChart.setAnimated(false);
				user_count_charts.put(hostname, userCountBarChart);
			}
			
			// create the stage for each host
			if(!user_count_stages.containsKey(hostname)) {
				Stage stage = new Stage();
				user_count_stages.put(hostname, stage);
			}
			
			// create the scene for each host
			if(!user_count_scenes.containsKey(hostname)) {
				Scene scene = new Scene(user_count_charts.get(hostname), WriteUserChartsTask.SCENE_WIDTH, WriteUserChartsTask.SCENE_HEIGHT);
				scene.getStylesheets().add(getClass().getResource(WriteUserChartsTask.CSS_FILE_NAME).toExternalForm());
				user_count_scenes.put(hostname, scene);
				
				user_count_stages.get(hostname).setScene(scene);
			}
			
			// always update the series of the charts!
			user_count_charts.get(hostname).getData().clear();
			user_count_charts.get(hostname).getData().add(series);
			
			ImageFileWriter.writeToFile(user_count_scenes.get(hostname), hostname, WriteUserChartsTask.FILE_NAME_PREFIX);
			
		});
		
		//System.out.println("USER COUNT DATA RENDERED AT " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond() + "\n\n");
	}
	
	private BarChart<String, Number> initializeUserCountBarChart(String title, String yAxisLabel, String xAxisLabel) {
		BarChart<String, Number> userCountBarChart;
		
		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel(xAxisLabel);

		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel(yAxisLabel);

		userCountBarChart = new BarChart<>(xAxis, yAxis);
		userCountBarChart.setTitle(title);
		
		return userCountBarChart;
	}
}