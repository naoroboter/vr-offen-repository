/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator;

/**
 *
 * @author xiaolong
 */
public class RandomHelper {
	public static int getRandomNumber(int a, int b) {
		if (b < a) {
			return getRandomNumber(b, a);
		}
		return a + (int) ((1 + b - a) * Math.random());
	}
}
