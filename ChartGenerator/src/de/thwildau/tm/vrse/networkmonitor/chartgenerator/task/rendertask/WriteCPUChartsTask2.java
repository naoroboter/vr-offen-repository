/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.rendertask;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.*;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.ImageFileWriter;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.readtask.ReadCPUDataTask2;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;

/**
 *
 * @author xiaolong
 */
public class WriteCPUChartsTask2 extends TimerTask {
	
	public static final long WRITE_CPU_CHARTS_INTERVAL = 10 * 1000;
	
	private static final int SCENE_HEIGHT = 480;
	private static final int SCENE_WIDTH = 1280;
	
	private static final String FILE_NAME_PREFIX = "cpu-usage-";
	
	@Override
	public void run() {
		/*
		Using: Platform.runLater(new Runnable() {...});
		is necessary to run the rendering part of the TimerTask on the JavaFX Application Thread!
		*/
        Platform.runLater(new Runnable() {
			@Override
            public void run() {
				//System.out.println("TIMER TASK at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
				// render work
				renderCPULoadCharts();
			}
		});
	}
	
	/**
	 * This method renders a CPU usage chart for all hosts and saves the rendered chart as a PNG file.
	 */
	private void renderCPULoadCharts() {
		
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			String hostname = result.getIdentity().getHostName();
			
			XYChart.Series<String, Number> series = new Series<>();
			
			// for each host create its own series of data for its chart
			ReadCPUDataTask2.cpu_usage_data.get(hostname).entrySet().forEach((cpuUsageDataEntry) -> {
				XYChart.Data chartData = new XYChart.Data(cpuUsageDataEntry.getKey(), cpuUsageDataEntry.getValue());
				series.getData().add(chartData);
				series.setName("Linux CPU Load");
			});
			
			// create the chart for each host
			if(!ReadCPUDataTask2.cpu_usage_charts.containsKey(hostname)) {
				AreaChart<String, Number> cpuUsageAreaChart = initializeCPUUsageChart("CPU Usage of " + hostname, "Usage in %", "Time in HH:mm:ss");
				cpuUsageAreaChart.getData().clear();
				cpuUsageAreaChart.getData().add(series);

				cpuUsageAreaChart.setLegendVisible(true);
				cpuUsageAreaChart.setLegendSide(Side.BOTTOM);

				cpuUsageAreaChart.setAnimated(false);
				cpuUsageAreaChart.setCreateSymbols(false);
				ReadCPUDataTask2.cpu_usage_charts.put(hostname, cpuUsageAreaChart);
			}
			
			// create the stage for each host
			if(!ReadCPUDataTask2.cpu_usage_stages.containsKey(hostname)) {
				Stage stage = new Stage();
				ReadCPUDataTask2.cpu_usage_stages.put(hostname, stage);
			}
			
			// create the scene for each host
			if(!ReadCPUDataTask2.cpu_usage_scenes.containsKey(hostname)) {
				Scene scene = new Scene(ReadCPUDataTask2.cpu_usage_charts.get(hostname), WriteCPUChartsTask2.SCENE_WIDTH, WriteCPUChartsTask2.SCENE_HEIGHT);
				scene.getStylesheets().add(getClass().getResource("line-chart-style.css").toExternalForm());
				ReadCPUDataTask2.cpu_usage_scenes.put(hostname, scene);
				
				ReadCPUDataTask2.cpu_usage_stages.get(hostname).setScene(scene);
			}
			
			// always update the series of the charts!
			ReadCPUDataTask2.cpu_usage_charts.get(hostname).getData().clear();
			ReadCPUDataTask2.cpu_usage_charts.get(hostname).getData().add(series);
			
			ImageFileWriter.writeToFile(ReadCPUDataTask2.cpu_usage_scenes.get(hostname), hostname, WriteCPUChartsTask2.FILE_NAME_PREFIX);
			
		});
		
		//System.out.println("CPU DATA RENDERED at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond() + "\n\n");
	}
	
	/**
	 * This method initializes a CPU usage chart.
	 * @param title the title of the chart
	 * @param yAxisLabel the label of the y-axis
	 * @param xAxisLabel the label of the x-axis
	 */
	private AreaChart<String, Number> initializeCPUUsageChart(String title, String yAxisLabel, String xAxisLabel) {
		AreaChart<String, Number> cpuUsageAreaChart;
		
		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel(xAxisLabel);

		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel(yAxisLabel);

		cpuUsageAreaChart = new AreaChart<>(xAxis, yAxis);
		cpuUsageAreaChart.setTitle(title);
		
		return cpuUsageAreaChart;
	}
}