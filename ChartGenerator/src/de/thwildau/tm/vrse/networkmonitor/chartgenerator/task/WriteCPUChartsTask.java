/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.ImageFileWriter;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.TimeHelper;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.util.MaxSizeHashMap;
import de.thwildau.tm.vrse.networkmonitor.model.Result;
import java.util.HashMap;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;

/**
 *
 * @author xiaolong
 */
public class WriteCPUChartsTask extends TimerTask {
	
	private static final int MAX_MAP_CAPACITY = 400;
	public static final long WRITE_CPU_CHARTS_INTERVAL = 5 * 1000; // x * 1000ms = x seconds
	
	// each host gets its own stage, scene, areachart and cpu-data-series
	private static final HashMap<String, Scene> cpu_usage_scenes = new HashMap<>();
	private static final HashMap<String, AreaChart<String, Number>> cpu_usage_charts = new HashMap<>();
	private static final HashMap<String, Stage> cpu_usage_stages = new HashMap<>();
	private static final HashMap<String, MaxSizeHashMap<String, Double>> cpu_usage_data = new HashMap<>();
	
	private static final int SCENE_HEIGHT = 480;
	private static final int SCENE_WIDTH = 1280;
	
	private static final String FILE_NAME_PREFIX = "cpu-usage-";
	
	@Override
	public void run() {
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			readCPUDataFromResult(result);
		});
		
		/*
		Using: Platform.runLater(new Runnable() {...});
		is necessary to run the rendering part of the TimerTask on the JavaFX Application Thread!
		*/
        Platform.runLater(new Runnable() {
			@Override
            public void run() {
				//System.out.println("TIMER TASK at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
				// render work
				renderCPULoadCharts();
			}
		});
	}
	
	
	private void readCPUDataFromResult(Result result) {
		//System.out.println("CPU DATA read at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
		
		String hostname = result.getIdentity().getHostName();
		if(!cpu_usage_data.containsKey(hostname)) {
			//System.out.println("Reading CPU usage date of host: " + hostname);
			MaxSizeHashMap<String, Double> cpu_usage_maxSizeHashMap = new MaxSizeHashMap<>(WriteCPUChartsTask.MAX_MAP_CAPACITY);
			cpu_usage_data.put(hostname, cpu_usage_maxSizeHashMap);
		}
		
		if(result.getResource().getCpu() != null) {
			if(result.getResource().getCpu().getLoadAverage().get1min() != -1) {
				// add value to the map of this host
				cpu_usage_data.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						result.getResource().getCpu().getLoadAverage().get1min()
				);
			} else {
				//System.out.println("Writing 0 as value");
				cpu_usage_data.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						new Double(0)
				);
			}
		}
	}
	
	/**
	 * This method renders a CPU usage chart for all hosts and saves the rendered chart as a PNG file.
	 */
	private void renderCPULoadCharts() {
		
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			String hostname = result.getIdentity().getHostName();
			
			XYChart.Series<String, Number> series = new Series<>();
			
			// for each host create its own series of data for its chart
			cpu_usage_data.get(hostname).entrySet().forEach((cpuUsageDataEntry) -> {
				XYChart.Data chartData = new XYChart.Data(cpuUsageDataEntry.getKey(), cpuUsageDataEntry.getValue());
				series.getData().add(chartData);
				series.setName("Linux CPU Load");
			});
			
			// create the chart for each host
			if(!cpu_usage_charts.containsKey(hostname)) {
				AreaChart<String, Number> cpuUsageAreaChart = initializeCPUUsageChart("CPU Usage of " + hostname, "Usage in %", "Time in HH:mm:ss");
				cpuUsageAreaChart.getData().clear();
				cpuUsageAreaChart.getData().add(series);

				cpuUsageAreaChart.setLegendVisible(true);
				cpuUsageAreaChart.setLegendSide(Side.BOTTOM);

				cpuUsageAreaChart.setAnimated(false);
				cpuUsageAreaChart.setCreateSymbols(false);
				cpu_usage_charts.put(hostname, cpuUsageAreaChart);
			}
			
			// create the stage for each host
			if(!cpu_usage_stages.containsKey(hostname)) {
				Stage stage = new Stage();
				cpu_usage_stages.put(hostname, stage);
			}
			
			// create the scene for each host
			if(!cpu_usage_scenes.containsKey(hostname)) {
				Scene scene = new Scene(cpu_usage_charts.get(hostname), WriteCPUChartsTask.SCENE_WIDTH, WriteCPUChartsTask.SCENE_HEIGHT);
				scene.getStylesheets().add(getClass().getResource("line-chart-style.css").toExternalForm());
				cpu_usage_scenes.put(hostname, scene);
				
				cpu_usage_stages.get(hostname).setScene(scene);
			}
			
			// always update the series of the charts!
			cpu_usage_charts.get(hostname).getData().clear();
			cpu_usage_charts.get(hostname).getData().add(series);
			
			ImageFileWriter.writeToFile(cpu_usage_scenes.get(hostname), hostname, WriteCPUChartsTask.FILE_NAME_PREFIX);
			
		});
		
		//System.out.println("CPU DATA RENDERED at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond() + "\n\n");
	}
	
	/**
	 * This method initializes a CPU usage chart.
	 * @param title the title of the chart
	 * @param yAxisLabel the label of the y-axis
	 * @param xAxisLabel the label of the x-axis
	 */
	private AreaChart<String, Number> initializeCPUUsageChart(String title, String yAxisLabel, String xAxisLabel) {
		AreaChart<String, Number> cpuUsageAreaChart;
		
		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel(xAxisLabel);

		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel(yAxisLabel);

		cpuUsageAreaChart = new AreaChart<>(xAxis, yAxis);
		cpuUsageAreaChart.setTitle(title);
		
		return cpuUsageAreaChart;
	}
}