/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.ImageFileWriter;
import de.thwildau.tm.vrse.networkmonitor.model.Result;
import java.util.HashMap;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;

/**
 *
 * @author xiaolong
 */
public class WriteDiskChartsTask extends TimerTask {
	
	public static final long WRITE_DISK_CHARTS_INTERVAL = 10000;
	
	// each host gets its own stage, scene, areachart and hdd-data-series
	private static final HashMap<String, Scene> disk_usage_scenes = new HashMap<>();
	private static final HashMap<String, PieChart> disk_usage_charts = new HashMap<>();
	private static final HashMap<String, Stage> disk_usage_stages = new HashMap<>();
	private static final HashMap<String, Double> disk_usage_data = new HashMap<>();
	
	private static final int SCENE_HEIGHT = 480;
	private static final int SCENE_WIDTH = 640;

	private static final String FILE_NAME_PREFIX = "disk-usage-";
	
	@Override
	public void run() {
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			readDiskDataFromResult(result);
		});
		/*
		Using: Platform.runLater(new Runnable() {...});
		is necessary to run the TimerTask on the JavaFX Application Thread!
		*/
        Platform.runLater(new Runnable() {
			@Override
            public void run() {
				//System.out.println("HDD TIMER TASK at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
				// render work
				renderDiskUsageCharts();
			}
        });
	}
	
	/**
	 * This method reads the disk usage data of one host from a Result.
	 * @param result the Result (JSON data put into objects) of which the disk usage data will be read
	 */
	private void readDiskDataFromResult(Result result) {
		//System.out.println("HDD DATA read at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
		
		String hostname = result.getIdentity().getHostName();
		
		if(result.getResource().getDisk() != null) { // if there is disk data
			if(result.getResource().getDisk().getFreeSpacePercent() != -1) { // and it's valid disk data
				disk_usage_data.put(hostname, new Double(result.getResource().getDisk().getFreeSpacePercent()));
			} else {
				disk_usage_data.put(hostname, new Double(0));
			}
		}
	}
	
	/**
	 * This method renders the disk usage pie chart for each host.
	 */
	private void renderDiskUsageCharts() {
		
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> { // for each host
			
			String hostname = result.getIdentity().getHostName();
			
			// create the chart for each host
			if(!disk_usage_charts.containsKey(hostname)) {
				disk_usage_charts.put(hostname, initializeDiskUsagePieChart("Disk Usage of " + hostname));
			}
			
			// update data for each host
			disk_usage_charts.get(hostname).getData().clear();
			
			if(disk_usage_data.containsKey(hostname)) {
				disk_usage_charts.get(hostname).getData().add(new PieChart.Data("used", disk_usage_data.get(hostname)));
				disk_usage_charts.get(hostname).getData().add(new PieChart.Data("empty", 100.0 - disk_usage_data.get(hostname)));
			} else {
				disk_usage_charts.get(hostname).getData().add(new PieChart.Data("used", 0));
				disk_usage_charts.get(hostname).getData().add(new PieChart.Data("empty", 0));
			}
			
			// create the stage for each host
			if(!disk_usage_stages.containsKey(hostname)) {
				Stage stage = new Stage();
				disk_usage_stages.put(hostname, stage);
			}
			
			// create the scene for each host
			if(!disk_usage_scenes.containsKey(hostname)) {
				Scene scene = new Scene(disk_usage_charts.get(hostname), WriteDiskChartsTask.SCENE_WIDTH, WriteDiskChartsTask.SCENE_HEIGHT);
				scene.getStylesheets().add(getClass().getResource("disk-usage-pie-chart-style.css").toExternalForm());
				disk_usage_scenes.put(hostname, scene);
				
				disk_usage_stages.get(hostname).setScene(scene);
			}
			
			ImageFileWriter.writeToFile(disk_usage_scenes.get(hostname), hostname, WriteDiskChartsTask.FILE_NAME_PREFIX);
		});
		
		//System.out.println("DISK DATA RENDERED at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond() + "\n\n");
	}
	
	/**
	 * This method initializes a disk usage chart.
	 * @param title the title of the disk usage data chart
	 * @return an initialized PieChart
	 */
	private PieChart initializeDiskUsagePieChart(String title) {
		PieChart diskUsagePieChart = new PieChart();
		diskUsagePieChart.setTitle(title);
		
		diskUsagePieChart.setAnimated(false);
		diskUsagePieChart.setLegendVisible(true);
		diskUsagePieChart.setLegendSide(Side.BOTTOM);
		
		return diskUsagePieChart;
	}
}

