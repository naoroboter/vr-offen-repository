package de.thwildau.tm.vrse.networkmonitor.chartgenerator;


import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;

public class ImageFileWriterRunnable implements Runnable {
	
	private final String fileName;
	private final String fileEnding;
	private final WritableImage snapShot;
	private final CountDownLatch latch;
	
	private static final String DESTINATION_DIRECTORY = "../NetworkMonitoring/assets/Textures/Charts/";
	
	public ImageFileWriterRunnable(CountDownLatch latch, String fileName, String fileEnding, WritableImage snapShot) {
		this.fileName = fileName;
		this.fileEnding = fileEnding;
		this.snapShot = snapShot;
		this.latch = latch;
	}
	
	@Override
	public void run() {
		try {
			ImageIO.write(
					SwingFXUtils.fromFXImage(snapShot, null),
					fileEnding,
					new File(ImageFileWriterRunnable.DESTINATION_DIRECTORY + fileName + "." + fileEnding)
			);
		} catch (IOException ex) {
			Logger.getLogger(ImageFileWriterRunnable.class.getName()).log(Level.SEVERE, null, ex);
		}
		latch.countDown();
		//System.out.println("Wrote chart image file " + fileName + "." + fileEnding);
	}
}