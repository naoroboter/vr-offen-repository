package de.thwildau.tm.vrse.networkmonitor.chartgenerator;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.WebserviceDataGetterTask;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.WriteCPUChartsTask;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.WriteDiskChartsTask;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.WritePingChartsTask;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.WriteUserChartsTask;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.readtask.ReadCPUDataTask2;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.rendertask.WriteCPUChartsTask2;
import java.util.Timer;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * An interval < 5s wont bring any improvement, since nagios doesn't update its data at a higher frequency than 5s!!!
 * @author xiaolong
 */
public class ChartGenerator extends Application {
	
	private Timer timer;
	
	public static void main(String[] args) {
		ChartGenerator.launch(ChartGenerator.class);
	}

	@Override
	public void start(Stage stage) throws Exception {
		timer = new Timer();
		
		// start the TimerTask, which gathers the data from the web service
		timer.scheduleAtFixedRate(new WebserviceDataGetterTask(), 0, WebserviceDataGetterTask.GET_DATA_INTERVAL);
		
		// start the TimerTasks, which render the charts
		// This is done in separate TimerTasks, to allow the TimerTasks to have differing intervals of rendering and saving image files.
		timer.scheduleAtFixedRate(new WriteCPUChartsTask(), 1000, WriteCPUChartsTask.WRITE_CPU_CHARTS_INTERVAL);
		
		//timer.scheduleAtFixedRate(new WriteCPUChartsTask(), 1000, ReadCPUDataTask2.READ_CPU_DATA_INTERVAL);
		//timer.scheduleAtFixedRate(new WriteCPUChartsTask(), 6000, WriteCPUChartsTask2.WRITE_CPU_CHARTS_INTERVAL);
		
		timer.scheduleAtFixedRate(new WriteDiskChartsTask(), 2000, WriteDiskChartsTask.WRITE_DISK_CHARTS_INTERVAL);
		timer.scheduleAtFixedRate(new WriteUserChartsTask(), 3000, WriteUserChartsTask.WRITE_USER_CHARTS_INTERVAL);
		timer.scheduleAtFixedRate(new WritePingChartsTask(), 4000, WritePingChartsTask.WRITE_PING_CHARTS_INTERVAL);
	}
}
