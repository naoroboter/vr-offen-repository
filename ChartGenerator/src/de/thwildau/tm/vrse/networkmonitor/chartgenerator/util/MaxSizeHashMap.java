/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.util;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author xiaolong
 * @param <K> type of the keys
 * @param <V> type of the values
 */
public class MaxSizeHashMap<K, V> extends LinkedHashMap<K, V> {
    private final int maxSize;

    public MaxSizeHashMap(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > maxSize;
    }
}