/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.readtask;

import de.thwildau.tm.vrse.networkmonitor.chartgenerator.task.*;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.TimeHelper;
import de.thwildau.tm.vrse.networkmonitor.chartgenerator.util.MaxSizeHashMap;
import de.thwildau.tm.vrse.networkmonitor.model.Result;
import java.util.HashMap;
import java.util.TimerTask;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.stage.Stage;

/**
 *
 * @author xiaolong
 */
public class ReadCPUDataTask2 extends TimerTask {
	
	public static final int MAX_MAP_CAPACITY = 400;
	public static final long READ_CPU_DATA_INTERVAL = 5 * 1000;
	
	// each host gets its own stage, scene, areachart and cpu-data-series
	public static final HashMap<String, Scene> cpu_usage_scenes = new HashMap<>();
	public static final HashMap<String, AreaChart<String, Number>> cpu_usage_charts = new HashMap<>();
	public static final HashMap<String, Stage> cpu_usage_stages = new HashMap<>();
	public static final HashMap<String, MaxSizeHashMap<String, Double>> cpu_usage_data = new HashMap<>();
	
	@Override
	public void run() {
		WebserviceDataGetterTask.getResultList().stream().forEach(result -> {
			readCPUDataFromResult(result);
		});
	}
	
	
	private void readCPUDataFromResult(Result result) {
		//System.out.println("CPU DATA read at " + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
		
		String hostname = result.getIdentity().getHostName();
		if(!cpu_usage_data.containsKey(hostname)) {
			//System.out.println("Reading CPU usage date of host: " + hostname);
			MaxSizeHashMap<String, Double> cpu_usage_maxSizeHashMap = new MaxSizeHashMap<>(ReadCPUDataTask2.MAX_MAP_CAPACITY);
			cpu_usage_data.put(hostname, cpu_usage_maxSizeHashMap);
		}
		
		if(result.getResource().getCpu() != null) {
			if(result.getResource().getCpu().getLoadAverage().get1min() != -1) {
				// add value to the map of this host
				cpu_usage_data.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						result.getResource().getCpu().getLoadAverage().get1min()
				);
			} else {
				//System.out.println("Writing 0 as value");
				cpu_usage_data.get(hostname).put(
						TimeHelper.getCurrentTimeString(), 
						new Double(0)
				);
			}
		}
	}
}