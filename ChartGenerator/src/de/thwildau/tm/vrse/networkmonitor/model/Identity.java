package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"host_name",
"ip",
"online"
})
public class Identity {

@JsonProperty("host_name")
private String hostName;
@JsonProperty("ip")
private String ip;
@JsonProperty("online")
private Boolean online;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The hostName
*/
@JsonProperty("host_name")
public String getHostName() {
return hostName;
}

/**
* 
* @param hostName
* The host_name
*/
@JsonProperty("host_name")
public void setHostName(String hostName) {
this.hostName = hostName;
}

/**
* 
* @return
* The ip
*/
@JsonProperty("ip")
public String getIp() {
return ip;
}

/**
* 
* @param ip
* The ip
*/
@JsonProperty("ip")
public void setIp(String ip) {
this.ip = ip;
}

/**
* 
* @return
* The online
*/
@JsonProperty("online")
public Boolean getOnline() {
return online;
}

/**
* 
* @param online
* The online
*/
@JsonProperty("online")
public void setOnline(Boolean online) {
this.online = online;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}