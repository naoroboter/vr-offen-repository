package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"cpu",
"disk",
"network",
"users"
})
public class Resource {

@JsonProperty("cpu")
private Cpu cpu;
	
@JsonProperty("disk")
private Disk disk;
@JsonProperty("network")
private Network network;
@JsonProperty("users")
private Users users;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return - the Cpu-Objekt
* The cpu-load
*/
@JsonProperty("cpu")
public Cpu getCpu() {
return cpu;
}

/**
* 
* @param - cpu
* The cpu to set
*/
@JsonProperty("cpu")
public void setCpu(Cpu cpu) {
this.cpu = cpu;
}

/**
* 
* @return
* The disk
*/
@JsonProperty("disk")
public Disk getDisk() {
return disk;
}

/**
* 
* @param disk
* The disk
*/
@JsonProperty("disk")
public void setDisk(Disk disk) {
this.disk = disk;
}

/**
* 
* @return
* The network
*/
@JsonProperty("network")
public Network getNetwork() {
return network;
}

/**
* 
* @param network
* The network
*/
@JsonProperty("network")
public void setNetwork(Network network) {
this.network = network;
}

/**
* 
* @return
* The users
*/
@JsonProperty("users")
public Users getUsers() {
return users;
}

/**
* 
* @param users
* The users
*/
@JsonProperty("users")
public void setUsers(Users users) {
this.users = users;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}