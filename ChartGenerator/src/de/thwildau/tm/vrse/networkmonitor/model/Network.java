package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"latency",
"loss_percent",
"status"
})
public class Network {

@JsonProperty("latency")
private Double latency;
@JsonProperty("loss_percent")
private Integer lossPercent;
@JsonProperty("status")
private String status;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The latency
*/
@JsonProperty("latency")
public Double getLatency() {
return latency;
}

/**
* 
* @param latency
* The latency
*/
@JsonProperty("latency")
public void setLatency(Double latency) {
	this.latency = latency;
}

/**
* 
* @return
* The lossPercent
*/
@JsonProperty("loss_percent")
public Integer getLossPercent() {
return lossPercent;
}

/**
* 
* @param lossPercent
* The loss_percent
*/
@JsonProperty("loss_percent")
public void setLossPercent(Integer lossPercent) {
this.lossPercent = lossPercent;
}

/**
* 
* @return
* The status
*/
@JsonProperty("status")
public String getStatus() {
return status;
}

/**
* 
* @param status
* The status
*/
@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}