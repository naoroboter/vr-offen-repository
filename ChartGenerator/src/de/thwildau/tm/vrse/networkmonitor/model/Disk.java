package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"free_inodes",
"free_inodes_percent",
"free_space",
"free_space_percent",
"status"
})
public class Disk {

@JsonProperty("free_inodes")
private String freeInodes;
@JsonProperty("free_inodes_percent")
private String freeInodesPercent;
@JsonProperty("free_space")
private String freeSpace;
@JsonProperty("free_space_percent")
private Integer freeSpacePercent;
@JsonProperty("status")
private String status;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The freeInodes
*/
@JsonProperty("free_inodes")
public String getFreeInodes() {
return freeInodes;
}

/**
* 
* @param freeInodes
* The free_inodes
*/
@JsonProperty("free_inodes")
public void setFreeInodes(String freeInodes) {
this.freeInodes = freeInodes;
}

/**
* 
* @return
* The freeInodesPercent
*/
@JsonProperty("free_inodes_percent")
public String getFreeInodesPercent() {
return freeInodesPercent;
}

/**
* 
* @param freeInodesPercent
* The free_inodes_percent
*/
@JsonProperty("free_inodes_percent")
public void setFreeInodesPercent(String freeInodesPercent) {
this.freeInodesPercent = freeInodesPercent;
}

/**
* 
* @return
* The freeSpace
*/
@JsonProperty("free_space")
public String getFreeSpace() {
return freeSpace;
}

/**
* 
* @param freeSpace
* The free_space
*/
@JsonProperty("free_space")
public void setFreeSpace(String freeSpace) {
this.freeSpace = freeSpace;
}

/**
* 
* @return
* The freeSpacePercent
*/
@JsonProperty("free_space_percent")
public Integer getFreeSpacePercent() {
return freeSpacePercent;
}

/**
* 
* @param freeSpacePercent
* The free_space_percent
*/
@JsonProperty("free_space_percent")
public void setFreeSpacePercent(Integer freeSpacePercent) {
this.freeSpacePercent = freeSpacePercent;
}

/**
* 
* @return
* The status
*/
@JsonProperty("status")
public String getStatus() {
return status;
}

/**
* 
* @param status
* The status
*/
@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}