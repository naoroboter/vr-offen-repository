package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"15min",
"1min",
"5min"
})
public class LoadAverage {

@JsonProperty("15min")
private Double _15min;
@JsonProperty("1min")
private Double _1min;
@JsonProperty("5min")
private Double _5min;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
*
* @return
* The _15min
*/
@JsonProperty("15min")
public Double get15min() {
return _15min;
}

/**
*
* @param _15min
* The 15min
*/
@JsonProperty("15min")
public void set15min(Double _15min) {
this._15min = _15min;
}

/**
*
* @return
* The _1min
*/
@JsonProperty("1min")
public Double get1min() {
return _1min;
}

/**
*
* @param _1min
* The 1min
*/
@JsonProperty("1min")
public void set1min(Double _1min) {
this._1min = _1min;
}

/**
*
* @return
* The _5min
*/
@JsonProperty("5min")
public Double get5min() {
return _5min;
}

/**
*
* @param _5min
* The 5min
*/
@JsonProperty("5min")
public void set5min(Double _5min) {
this._5min = _5min;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}