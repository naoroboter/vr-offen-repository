package de.thwildau.tm.vrse.networkmonitor.model;


import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"load average",
"status"
})
public class Cpu {

@JsonProperty("load average")
private LoadAverage loadAverage;
@JsonProperty("status")
private String status;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
*
* @return
* The loadAverage
*/
@JsonProperty("load average")
public LoadAverage getLoadAverage() {
return loadAverage;
}

/**
*
* @param loadAverage
* The load average
*/
@JsonProperty("load average")
public void setLoadAverage(LoadAverage loadAverage) {
this.loadAverage = loadAverage;
}

/**
*
* @return
* The status
*/
@JsonProperty("status")
public String getStatus() {
return status;
}

/**
*
* @param status
* The status
*/
@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}