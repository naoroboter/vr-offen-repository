package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"existing",
"logged in"
})
public class Count {

@JsonProperty("existing")
private String existing;
@JsonProperty("logged in")
private Integer loggedIn;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The existing
*/
@JsonProperty("existing")
public String getExisting() {
return existing;
}

/**
* 
* @param existing
* The existing
*/
@JsonProperty("existing")
public void setExisting(String existing) {
this.existing = existing;
}

/**
* 
* @return
* The loggedIn
*/
@JsonProperty("logged in")
public Integer getLoggedIn() {
return loggedIn;
}

/**
* 
* @param loggedIn
* The logged in
*/
@JsonProperty("logged in")
public void setLoggedIn(Integer loggedIn) {
this.loggedIn = loggedIn;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}