package de.thwildau.tm.vrse.networkmonitor.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"identity",
"resource"
})
public class Result {

@JsonProperty("identity")
private Identity identity;
@JsonProperty("resource")
private Resource resource;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The identity
*/
@JsonProperty("identity")
public Identity getIdentity() {
return identity;
}

/**
* 
* @param identity
* The identity
*/
@JsonProperty("identity")
public void setIdentity(Identity identity) {
this.identity = identity;
}

/**
* 
* @return
* The resource
*/
@JsonProperty("resource")
public Resource getResource() {
return resource;
}

/**
* 
* @param resource
* The resource
*/
@JsonProperty("resource")
public void setResource(Resource resource) {
this.resource = resource;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}