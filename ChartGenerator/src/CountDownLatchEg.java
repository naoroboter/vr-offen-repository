
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class CountDownLatchEg {

	public static void main(String[] args) {
		int threadCount = 8;
		CountDownLatch latch = new CountDownLatch(threadCount);
		System.out.println("Start all threads");
		for (int i = 0; i < threadCount; i++) {
			new Thread(new MyRunnable(latch, i)).start();
		}
		System.out.println("All threads started");
		try {
			latch.await();
		} catch (InterruptedException e) {
		}
		System.out.println("All threads finished");

	}
}

class MyRunnable implements Runnable {

	private CountDownLatch latch;
	private Random rand = new Random();
	private long delay;
	private int id;

	public MyRunnable(CountDownLatch latch, int id) {
		this.latch = latch;
		delay = (rand.nextInt(4) + 1) * 1000;
		this.id = id;
	}

	@Override
	public void run() {
		System.out.println("Start thread: " + id);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
		}
		System.out.println("End thread: " + id);
		latch.countDown();
	}
}
