/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thwildau.tm.vrse.networkmonitor.chartgenerator.util;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author xiaolong
 */
public class MaxSizeHashMapTest {
	
	MaxSizeHashMap<String, Number> maxSizeHashMap;
	
	public MaxSizeHashMapTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of removeEldestEntry method, of class MaxSizeHashMap.
	 */
	@Test
	public void testMaxSizeHashMap() {
		int maxSize = 10;
		maxSizeHashMap = new MaxSizeHashMap<>(maxSize);
		
		// add some entries
		for(int entry_number = 0; entry_number < maxSize; entry_number++) {
			maxSizeHashMap.put((""+entry_number), entry_number);
		}
		
		// is the size correct?
		assertEquals(10, maxSizeHashMap.size());
		
		// are the added entries correct?
		for(int index = 0; index < maxSize; index++) {
			assertEquals(index, maxSizeHashMap.get(""+index));
		}
		
		// add some more entries
		int addCount = 5;
		for(int count = 0; count < addCount; count++) {
			maxSizeHashMap.put("TEST" + count, 100 + count);
		}
		
		// when I add an entry, is the size still only the maximum size, or did it grow?
		assertEquals(10, maxSizeHashMap.size());
		
		// has the oldest entry been deleted and the new one been added?
		for(int index = 0; index < addCount; index++) {
			assertEquals(100 + index, maxSizeHashMap.get("TEST"+index));
		}
		for(int index = addCount; index < maxSize - addCount; index++) {
			assertEquals(100 + index, maxSizeHashMap.get(""+index));
		}
	}
	
}
